<?php
return [
    'index' => env('APP_INDICE', 0.43),
    'distribution' => [
    	'rev' => env('DISTL1', 0.08),
    	'father' => env('DISTL2', 0.04),
    	'grandfather' => env('DISTL3', 0.02),
    	'ggrandfather' => env('DISTL4', 0.01),
    ],
    'plans' => [
    	'unikenko' => [
    		'1+6' => [
	    		'entry' => 0.75,
	    		'parcels' => 0.35
	    	],
	    	'1+12' => [
	    		'entry' => 0.65,
	    		'parcels' => 0.30
	    	],
	    	'1+18' => [
	    		'entry' => 0.60,
	    		'parcels' => 0.25
	    	],
	    	'1+24' => [
	    		'entry' => 0.50,
	    		'parcels' => 0.20
	    	],
    	],
    	'parceiro' => [
    		'1+6' => [
	    		'entry' => 0.50,
	    		'parcels' => 0.25
	    	],
	    	'1+12' => [
	    		'entry' => 0.40,
	    		'parcels' => 0.23
	    	],
	    	'1+18' => [
	    		'entry' => 0.30,
	    		'parcels' => 0.20
	    	],
	    	'1+24' => [
	    		'entry' => 0.20,
	    		'parcels' => 0.18
	    	],
    	]
    ]
];