<?php

namespace App\Domains\Users;

use App\Domains\Churches\Church;
use App\Domains\Maintainers\Maintainer;
use App\Domains\Order;
use App\Domains\Refs\Ref;
use App\Units\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Codecasts\Presenter\Presentable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Presentable, Notifiable, CanResetPassword, Order;

    protected $presenter = UserPresenter::class;

    public $timestamps = false;

    protected $fillable = [
        'name', 'email', 'documento', 'cpf', 'phone', 'status', 'code', 'street', 'number', 'neighborhood', 'city', 'uf', 'password', 'church_id', 'ref_id', 'profile', 'idpagarme', 'date', 'cep', 'motoqueiro_id', 'nascimento', 'complemento', 'obs', 'send_mail', 'send_phone'
    ];

    public function church()
    {
        return $this->belongsTo(Church::class, 'church_id');
    }

    public function motoqueiro()
    {
        return $this->belongsTo(User::class, 'motoqueiro_id');
    }

    public function maintainers()
    {
        return $this->hasMany(Maintainer::class, 'rider_id');
    }

    public function getMaintainersByList($list, $offer = false)
    {
        $maintainers = $this->maintainers()
                            ->where('list', $list)
                            ->active()
                            ->orderBy('neighborhood', 'ASC')
                            ->orderBy('name', 'ASC')
                            ->get();
        return $maintainers;
    }

    public function getValuesByList($list, $month)
    {
        $users = $this->maintainers()
                      ->where('list', $list)
                      ->active()->get();
        $arr = [];
        $arr['list'] = $list;
        $arr['month'] = $month;
        $arr['count'] = 0;
        $arr['paid'] = 0;
        $arr['waiting'] = 0;
        $arr['paidValue'] = 0;
        $arr['waitingValue'] = 0;

        foreach($users as $user) {
            $offerP = $user->offer($month);
            if ($offerP) {
                $arr['count']++;
                if ($offerP->status == 'pago') {
                    $arr['paidValue'] += $offerP->value;
                    $arr['paid']++;
                } else {
                    $arr['waitingValue'] += $user->value;
                    $arr['waiting']++;
                }
            }
        }
        $arr['paidPercent'] = round(($arr['paid'] * 100) / $arr['count'], 2);
        $arr['waitingPercent'] = round(($arr['waiting'] * 100) / $arr['count'], 2);
        
        return $arr;
    }

    public function getValuesByMonth($month)
    {
        $list = [5, 10, 15, 20, 25, 30];
        $arr = ['total' => [
            'count' => 0,
            'paid' => 0,
            'waiting' => 0,
            'paidValue' => 0,
            'paidPercent' => 0,
            'waitingValue' => 0,
            'waitingPercent' => 0
        ]];
        foreach ($list as $list) {
            $result = $this->getValuesByList($list,  $month);
            $arr[$list] = $result;
            $arr['total']['count'] += $result['count'];
            $arr['total']['paid'] += $result['paid'];
            $arr['total']['waiting'] += $result['waiting'];
            $arr['total']['paidValue'] += $result['paidValue'];
            $arr['total']['waitingValue'] += $result['waitingValue'];
        }
        $arr['total']['paidPercent'] = round(($arr['total']['paid'] * 100) / $arr['total']['count'], 2);
        $arr['total']['waitingPercent'] = round(($arr['total']['waiting'] * 100) / $arr['total']['count'], 2);
        
        return $arr;
    }

    public function ref()
    {
        return $this->belongsTo(Ref::class, 'ref_id');
    }

    public function isAdmin()
    {
        return $this->profile == 'Admin' ? true : false;
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeUser($query)
    {
        return $query->where('profile', 'mantenedor');
    }

    public function scopeAdmin($query)
    {
        return $query->where('profile', 'admin');
    }

    public function scopeRider($query)
    {
        return $query->where('profile', 'motoqueiro');
    }
}
