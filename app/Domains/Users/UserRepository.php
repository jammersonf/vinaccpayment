<?php

namespace App\Domains\Users;

use Prettus\Repository\Eloquent\BaseRepository;

class UserRepository extends BaseRepository
{
    public function customer($user)
    {
        $user = $this->find($user);
        return $user->idpagarme ? $this->getCustomer($user): $this->createCustomer($user);
    }

    function createCustomer($user)
    {
    	\Pagarme::setApiKey(config('api.pagarme.key'));
    	$arr = [
            'type' => 'individual',
            'country' => 'br',
            'name' => $user->name,
            'email' => $user->email,
            'document_number' => str_replace(['.', '-', '/'], '', $user->cpf),
            'document_type' => $user->documento
        ];

        $cliente = new \PagarMe_Customer($arr);
        $cliente->create();
        $idpagarme = $cliente->id;
        $user->idpagarme = $idpagarme;

        //atualiza user
        $this->update(['idpagarme' => $idpagarme], $user->id);

        return [
            'id' => $idpagarme,
            'name' => $user->name,
            'document_number' => str_replace(['.', '-'], '', $user->cpf)
        ];
    }

    function getCustomer($user)
    {
        return [
            'id' => $user->idpagarme,
            'name' => $user->name,
            'document_number' => str_replace(['.', '-'], '', $user->cpf)
        ];
    }

    function model()
    {
        return "App\\Domains\\Users\\User";
    }
    
}
