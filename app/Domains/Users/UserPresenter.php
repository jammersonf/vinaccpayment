<?php

namespace App\Domains\Users;

use Codecasts\Presenter\Presenter;
use Illuminate\Support\Str;

/**
 * Class UserPresenter.
 */
class UserPresenter extends Presenter
{
	public function date()
	{
		return date('d/m/Y', strtotime($this->entity->date));
	}
}
