<?php

namespace App\Domains\Slips;

use App\Domains\Slips\Slip;
use Prettus\Repository\Eloquent\BaseRepository;

class SlipRepository extends BaseRepository
{
    public function getByDatePaid($dt, $dt2 = false, $vale)
    {
        $math = $dt2 ? '>=' : '=';
        $slips = Slip::with(['user'])->where('datePaid', $math, $dt)->orderBy('datePaid', 'asc');
        if ($vale) {
            $slips = $slips->where('single', 2);
        } else {
            $slips = $slips->where('single', '!=', 2);
        }
        if ($dt2) {
            $slips = $slips->where('datePaid', '<=', $dt2)->get()->groupBy('datePaid');
        } else {
            $slips = $slips->get();
        }

        return $slips;
    }

    public function getByDate($dt, $dt2 = false, $vale)
    {
        $math = $dt2 ? '>=' : '=';
        $slips = Slip::where('date', $math, $dt)->whereNull('datePaid')->orderBy('date', 'asc');
        if ($vale) {
            $slips = $slips->where('single', 2);
        } else {
            $slips = $slips->where('single', '!=', 2);
        }
        if ($dt2) {
            $slips = $slips->where('date', '<=', $dt2)->get()->groupBy('date');
        } else {
            $slips = $slips->get();
        }

        return  $slips;
    }

    public function getByMonth($month)
    {
        $slips = Slip::with(['user' => function ($q) {
            $q->orderBy('name');
        }])
                        ->where('status', 'paid')
                        ->whereYear('datePaid', date('Y'))
                        ->whereMonth('datePaid', $month)
                        ->groupBy('user_id')
                        ->get();
        
        return $slips;
    }

    public function mountDataTable($dt, $dt2)
    {
        $datatable = \Lava::DataTable();

        $datatable->addDateColumn('date')
         ->addNumberColumn('Realizado')
         ->addNumberColumn('Previsto');

        $dt = \Carbon\Carbon::createFromFormat('Y-m-d', $dt);
        $dt2 = \Carbon\Carbon::createFromFormat('Y-m-d', $dt2);

        while ($dt->lte($dt2)) {
            $date = $dt->toDateString();
            $prev = $this->getByDate($date)->sum('value');
            $paid = $this->getByDatePaid($date)->sum('value');
            $datatable->addRow([$date, $paid, $prev]);
            $dt->addDay();
        }

        $lava = \Lava::ComboChart('Boletos', $datatable, [
            'title' => 'Previsto X Realizado',
            'titleTextStyle' => [
                'color'    => 'rgb(123, 65, 89)',
                'fontSize' => 16
            ],
            'legend' => [
                'position' => 'in'
            ],
            'seriesType' => 'bars',
            'series' => [
                1 => ['type' => 'line']
            ]
        ]);

        return $lava;
    }

    public function createSlipDate($input, $user, $customer, $single)
    {
        try {
            $date = $input['date'];
            $dateTransaction = date("$date\TH:i:s.v\Z");
            
            $transaction = $this->createTransaction($input['value'], $customer, $dateTransaction, 1, 1);
            $slip = $this->createSlip($transaction['return'], $user, 1, $input['value'], false, $single);
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function createSlipSingle($input, $user, $customer, $single)
    {
        try {
            $d = $input['day'] < 10 ? '0'.$input['day'] : $input['day'];
            $thisMonth = ($input['thisMonth'] && $d > date('d'));
            $j = 1;
            for ($i=1;$i<=$input['qtd'];$i++) {
                if ($i == 1 && $thisMonth) {
                    $dateTransaction = date("Y-m-$d\TH:i:s.v\Z");
                    $transaction = $this->createTransaction($input['value'], $customer, $dateTransaction, $i, $input['qtd']);
                    $slip = $this->createSlip($transaction['return'], $user, $i, $input['value'], false, $single);
                    $i++;
                }
                $dateTransaction = date("Y-m-$d\TH:i:s.v\Z", strtotime("+$j month"));
                $transaction = $this->createTransaction($input['value'], $customer, $dateTransaction, $i, $input['qtd']);
                $slip = $this->createSlip($transaction['return'], $user, $i, $input['value'], false, $single);
                $j++;
            }
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function createSlips($payment, $customer, $thisMonth = false)
    {
        try {
            $d = $payment->day < 10 ? '0'.$payment->day : $payment->day;
            $thisMonth = ($thisMonth && $d > date('d'));

            $j = 1;
            $envios = [];
            for ($i=1;$i<=12;$i++) {
                if ($i == 1 && $thisMonth) {
                    $dateTransaction = date("Y-m-$d");
                    $transaction = $this->createTransaction($payment->value, $customer, $dateTransaction, $i);
                    $envios[] = $transaction;
                    $slip = $this->createSlip($transaction['return'], $payment->user_id, $i, $payment->value, $payment->id, 0);
                    $i++;
                }
                $dateTransaction = date("Y-m-$d", strtotime("+$j month"));
                $transaction = $this->createTransaction($payment->value, $customer, $dateTransaction, $i);
                $envios[] = $transaction;
                $slip = $this->createSlip($transaction['return'], $payment->user_id, $i, $payment->value, $payment->id, 0);
                $j++;
            }
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function createSlip($transaction, $user, $i, $value, $idDayment, $single)
    {
        $arr = ['payment_id' => 0];
        $url = explode('/', $transaction->postback_url);
        if ($idDayment) {
            $arr = ['payment_id' => $idDayment];
        }
        $slip = [
            'value' => $value,
            'slip' => $i,
            'status' => $transaction->status,
            'date' => date('Y-m-d', strtotime($transaction->boleto_expiration_date)),
            'payment_id' => $arr['payment_id'],
            'transaction' => $transaction->id,
            'url' => end($url),
            'boleto_barcode' => $transaction->boleto_barcode,
            'boleto_url' => $transaction->boleto_url,
            'single' => $single,
            'user_id' => $user
        ];
        
        return $this->create($slip);
    }

    public function createTransaction($value, $customer, $date, $i, $qtd = 12)
    {
        $date = $this->checkFebruary($date);

        \Pagarme::setApiKey(config('api.pagarme.key'));
        $url = url('/slip/'.$this->setUrl($customer, $i));
        $instruction = ($qtd > 1) ? "Boleto $i de $qtd" : '';
        $instruction = $instruction.' | Sr. Caixa, aceitar o pagamento e não cobrar juros após o vencimento.';
        $arr = [
            'amount' => str_replace('.', '', $value),
            'payment_method' => 'boleto',
            'postback_url' => $url,
            'customer' => $customer,
            'boleto_instructions' => $instruction,
            'boleto_expiration_date' => $date,
            'async' => false
        ];

        $transaction = new \PagarMe_Transaction($arr);
        $transaction->charge();

        return ['send' => $arr, 'return' => $transaction];
    }

    public function checkFebruary($date)
    {
        $dateArr = explode('-', $date);
        $time = '03:00';
        if ($dateArr[1] == '02' && $dateArr[2] >= '28') {
            $date = $dateArr[0].'-02-28';
        }
        if ($dateArr[1] == '11' or $dateArr[1] == '12' or $dateArr[1] == '01') {
            $time = '02:00';
        }
        return date("Y-m-d\T$time:s.v\Z", strtotime($date));
    }

    public function setUrl($customer, $i)
    {
        return hash('ripemd160', $customer['id'].$i);
    }

    public function model()
    {
        return "App\\Domains\\Slips\\Slip";
    }
}
