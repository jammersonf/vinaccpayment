<?php

namespace App\Domains\Slips;

use App\Domains\Payments\Payment;
use App\Domains\SlipLogs\SlipLog;
use App\Domains\Users\User;
use Codecasts\Presenter\Presentable;
use Illuminate\Database\Eloquent\Model;

class Slip extends Model
{
    use Presentable;
    
    protected $presenter = SlipPresenter::class;
    
    public $timestamps = false;

    protected $fillable = [
        'value', 'slip', 'status', 'date', 'datePaid', 'payment_id', 'obs', 'transaction', 'url', 'boleto_barcode', 'boleto_url', 'single', 'user_id', 'download'
    ];

    public function payment()
    {
        return $this->belongsTo(Payment::class, 'payment_id');
    }

    public function logs()
    {
        return $this->hasMany(SlipLog::class, 'slips_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeSingles($query)
    {
        return $query->where('single', 1);
    }

    public function scopeOffers($query)
    {
        return $query->where('single', 2);
    }

    public function scopeByUser($query, $id)
    {
        return $query->where('user_id', $id);
    }

    public function scopeUnpaid($query)
    {
        return $query->where('status', 'waiting_payment');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($slip) {
            foreach ($slip->logs as $log) {
                $log->delete();
            }
        });
    }
}
