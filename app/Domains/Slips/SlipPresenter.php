<?php

namespace App\Domains\Slips;

use Codecasts\Presenter\Presenter;
use Illuminate\Support\Str;

/**
 * Class SlipPresenter.
 */
class SlipPresenter extends Presenter
{
	public function value()
	{
		return number_format($this->entity->value, 2, ',', '.');
	}

	public function date()
	{
		return date('d/m/Y', strtotime($this->entity->date));
	}

	public function datePaid()
	{
		return $this->entity->datePaid ? date('d/m/Y', strtotime($this->entity->datePaid)) : '';
	}

	public function status()
	{
		$msg = '';
		$dtVenc = \Carbon\Carbon::createFromFormat('Y-m-d', $this->entity->date)->addDay();
		$dtnow = \Carbon\Carbon::now();
		switch ($this->entity->status) {
			case 'waiting_payment':
				$msg = 'Aguardando pagamento';
				break;
			case 'paid':
				$msg = 'Pago';
				break;			
			default:
				$msg = 'Processando';
				break;
		}

		$msg = ($dtnow->gt($dtVenc) && $this->entity->status == 'waiting_payment') ? $msg.' (Atrasado)' : $msg;

		return $msg;
	}

	public function statusClass()
	{
		$msg = '';
		$dtVenc = \Carbon\Carbon::createFromFormat('Y-m-d', $this->entity->date)->addDay();
		$dtnow = \Carbon\Carbon::now();
		switch ($this->entity->status) {
			case 'paid':
				$msg = 'success';
				break;			
			default:
				$msg = '';
				break;
		}

		$msg = ($dtnow->gt($dtVenc) && $this->entity->status == 'waiting_payment') ? 'danger' : $msg;

		return $msg;
	}
}
