<?php

namespace App\Domains\Maintainers;

use App\Domains\OfferPayments\OfferPayment;
use App\Domains\Offers\Offer;
use App\Domains\Users\User;
use Illuminate\Database\Eloquent\Model;

class Maintainer extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'email', 'cpf', 'phone', 'status', 'street', 'number',
        'neighborhood', 'city', 'uf', 'cep', 'rider_id', 'value',
        'list', 'created_at', 'nascimento', 'complemento', 'obs'
    ];

    public function offers()
    {
        return $this->hasMany(Offer::class, 'maintainer_id');
    }

    public function offerPayments()
    {
        return $this->hasMany(OfferPayment::class, 'maintainer_id');
    }

    public function offer($month = '', $year = '')
    {
        $month = $month ?: date('m');
        $year = $year ?: date('Y');
        $offerPayment = $this->offerPayments()
                             ->whereYear('date_old', $year)
                             ->whereMonth('date_old', $month)
                             ->first();
        
        return $offerPayment;
    }

    public function getOfferAttribute()
    {
        return $this->offer();
    }

    public function rider()
    {
        return $this->belongsTo(User::class, 'rider_id');
    }

    public function scopeList($query, $list)
    {
        return $query->where('list', $list);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeBlocked($query)
    {
        return $query->where('status', 0);
    }

    public function checkDuplicity($input)
    {
        $get = $this->where('cpf', $input['cpf'])
                    ->orWhere('email', $input['email'])
                    ->first();

        return $get ? true : false;
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            $user->status = 1;
        });
    }
}
