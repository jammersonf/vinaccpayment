<?php

namespace App\Domains\OfferPaymentOption;

use App\Domains\Maintainers\Maintainer;
use App\Domains\OfferPayments\OfferPayment;
use App\Domains\Users\User;
use Illuminate\Database\Eloquent\Model;

class OfferPaymentOption extends Model
{
    public $timestamps = false;

    protected $table = 'offer_payment_options';

    protected $fillable = [
        'offer_payment_id', 'maintainer_id', 'rider_id', 'type', 'months', 'status', 'date', 'retorno'
    ];

    public function offerp()
    {
        return $this->belongsTo(OfferPayment::class, 'offer_payment_id');
    }

    public function maintainer()
    {
        return $this->belongsTo(Maintainer::class, 'maintainer_id');
    }

    public function rider()
    {
        return $this->belongsTo(User::class, 'rider_id');
    }

    public function scopeUnresolved()
    {
        return $this->where('status', 'pendente');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($offer) {
            $offer->date = date('Y-m-d H:i:s');
        });
    }
}
