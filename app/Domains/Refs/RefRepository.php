<?php

namespace App\Domains\Refs;

use Prettus\Repository\Eloquent\BaseRepository;

class RefRepository extends BaseRepository
{
    function model()
    {
        return "App\\Domains\\Refs\\Ref";
    }
}
