<?php

namespace App\Domains\Refs;

use App\Domains\Order;
use Codecasts\Presenter\Presentable;
use Illuminate\Database\Eloquent\Model;

class Ref extends Model
{
    use Presentable, Order;
    
    protected $presenter = RefPresenter::class;
    
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

}
