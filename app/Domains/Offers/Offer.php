<?php

namespace App\Domains\Offers;

use App\Domains\Maintainers\Maintainer;
use App\Domains\OfferPayments\OfferPayment;
use App\Domains\Users\User;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'maintainer_id', 'rider_id', 'list', 'year', 'cadastro'
    ];

    public function createPayments($maintainer, $renew = false)
    {
        $offer = $this->create([
            'maintainer_id' => $maintainer->id,
            'rider_id' => $maintainer->rider_id,
            'list' => $maintainer->list
        ]);
        
        for ($i=1;$i<=12;$i++) {
            $date = explode('-', $maintainer->created_at);
            $date = $date[0].'-'.$date[1].'-'.$maintainer->list;
            if ($renew) {
                $date = date('Y-m-').$maintainer->list;
            }
            
            $offer->payments()->create([
                'number' => $i,
                'date' => date('Y-m-d', strtotime("$date +$i month")),
                'date_old' => date('Y-m-d', strtotime("$date +$i month")),
                'maintainer_id' => $maintainer->id,
            ]);
        }
        $this->checkFebruary();
    }

    public function checkFebruary()
    {
        $offer = OfferPayment::where('date', '2020-03-01')
                             ->get();
        foreach ($offer as $o) {
            $o->date = date('Y').'-02-28';
            $o->date_old = date('Y').'-02-28';
            $o->save();
        }

        return true;
    }

    public function payments()
    {
        return $this->hasMany(OfferPayment::class, 'offer_id');
    }

    public function maintainer()
    {
        return $this->belongsTo(Maintainer::class, 'maintainer_id');
    }

    public function rider()
    {
        return $this->belongsTo(User::class, 'rider_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($offer) {
            $offer->year = date('Y');
            $offer->cadastro = date('Y-m-d H:i:s');
        });

        static::deleting(function ($offer) {
            $offer->payments()->delete();
        });
    }
}
