<?php

namespace App\Domains\Payments;

use Prettus\Repository\Eloquent\BaseRepository;

class PaymentRepository extends BaseRepository
{
    function model()
    {
        return "App\\Domains\\Payments\\Payment";
    }
}
