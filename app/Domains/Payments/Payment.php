<?php

namespace App\Domains\Payments;

use App\Domains\Users\User;
use App\Domains\Slips\Slip;
use Codecasts\Presenter\Presentable;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use Presentable;
    
    protected $presenter = PaymentPresenter::class;
    
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'day', 'year', 'value', 'status', 'date', 'download'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function slips()
    {
        return $this->hasMany(Slip::class, 'payment_id');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($payment) {
            foreach($payment->slips as $slip) {
                foreach($slip->logs as $log) {
                    $log->delete();
                }
                $slip->delete();
            }
        });
    }
}
