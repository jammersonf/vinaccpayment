<?php

namespace App\Domains\OfferPaymentReschedule;

use App\Domains\OfferPayments\OfferPayment;
use Illuminate\Database\Eloquent\Model;

class OfferPaymentReschedule extends Model
{
    public $timestamps = false;

    protected $table = 'offer_payment_reschedule';

    protected $fillable = [
        'offer_payment_id', 'rider_id', 'list', 'date'
    ];

    public function offerp()
    {
        return $this->belongsTo(OfferPayment::class, 'offer_payment_id');
    }

    public function scopeUnpaid()
    {
        return $this->whereHas('offerp', function ($query) {
            return $query->whereStatus('pendente');
        });
    }

    protected static function boot()
    {
        parent::boot();
    }
}
