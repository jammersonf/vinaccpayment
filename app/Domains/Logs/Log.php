<?php

namespace App\Domains\Logs;

use App\Domains\Payments\Payment;
use Codecasts\Presenter\Presentable;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use Presentable;
    
    protected $presenter = LogPresenter::class;
    
    public $timestamps = false;

    protected $fillable = [
        'type', 'text', 'slips_id', 'date'
    ];
}
