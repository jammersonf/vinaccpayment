<?php

namespace App\Domains;

trait Order
{
    public function scopeOrderId($query)
    {
        return $query->orderBy('id', 'desc');
    }

    public function scopeOrdername($query)
    {
        return $query->orderBy('name', 'asc');
    }
}
