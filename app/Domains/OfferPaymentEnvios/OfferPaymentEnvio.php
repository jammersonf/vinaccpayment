<?php

namespace App\Domains\OfferPaymentEnvios;

use App\Domains\Maintainers\Maintainer;
use App\Domains\OfferPayments\OfferPayment;
use Carbon\Carbon;
use Codecasts\Presenter\Presentable;
use Illuminate\Database\Eloquent\Model;

class OfferPaymentEnvio extends Model
{
    use Presentable;
    
    public $timestamps = false;

    protected $table = 'offer_payments_envios';

    protected $fillable = [
        'maintainer_id', 'payment_id', 'type', 'text', 'date'
    ];

    public function maintainer()
    {
        return $this->belongsTo(Maintainer::class, 'maintainer_id');
    }

    public function payment()
    {
        return $this->belongsTo(OfferPayment::class, 'payment_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            $user->date = date('Y-m-d H:i:s');
        });
    }
}
