<?php

namespace App\Domains\SlipLogs;

use App\Domains\Slips\Slip;
use App\Domains\Users\User;
use Codecasts\Presenter\Presentable;
use Illuminate\Database\Eloquent\Model;

class SlipLog extends Model
{
    use Presentable;
    
    protected $presenter = SlipLogPresenter::class;

    public $table = 'slips_logs';
    
    public $timestamps = false;

    protected $fillable = [
        'text', 'type', 'slips_id', 'date'
    ];

    public function slip()
    {
    	return $this->belongsTo(Slip::class, 'slips_id');
    }

    public function maintainer()
    {
        return User::find($this->slip->user_id);
    }

}
