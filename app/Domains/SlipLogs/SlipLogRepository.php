<?php

namespace App\Domains\SlipLogs;

use Prettus\Repository\Eloquent\BaseRepository;

class SlipLogRepository extends BaseRepository
{
    function model()
    {
        return "App\\Domains\\SlipLogs\\SlipLog";
    }
}
