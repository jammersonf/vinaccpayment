<?php

namespace App\Domains\Churches;

use Prettus\Repository\Eloquent\BaseRepository;

class ChurchRepository extends BaseRepository
{
    function model()
    {
        return "App\\Domains\\Churches\\Church";
    }
}
