<?php

namespace App\Domains\Churches;

use App\Domains\Order;
use Codecasts\Presenter\Presentable;
use Illuminate\Database\Eloquent\Model;

class Church extends Model
{
    use Presentable, Order;
    
    protected $presenter = ChurchPresenter::class;
    
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

}
