<?php

namespace App\Domains\OfferPayments;

use Codecasts\Presenter\Presenter;
use Illuminate\Support\Str;

/**
 * Class SlipPresenter.
 */
class OfferPaymentPresenter extends Presenter
{
    public function value()
    {
        return number_format($this->entity->value, 2, ',', '.');
    }

    public function date()
    {
        return date('d/m/Y', strtotime($this->entity->date));
    }

    public function paid()
    {
        return $this->entity->paid ? date('d/m/Y H:i', strtotime($this->entity->paid)) : '';
    }
}
