<?php

namespace App\Domains\OfferPayments;

use App\Domains\Maintainers\Maintainer;
use App\Domains\Offers\Offer;
use Carbon\Carbon;
use Codecasts\Presenter\Presentable;
use Illuminate\Database\Eloquent\Model;

class OfferPayment extends Model
{
    use Presentable;
    
    protected $presenter = OfferPaymentPresenter::class;
    
    public $timestamps = false;

    protected $fillable = [
        'offer_id', 'number', 'value', 'date', 'date_old', 'status', 'paid', 'maintainer_id'
    ];

    public function suspend($offerp, $months)
    {
        $offer = $offerp->offer;
        $data = $this->where('offer_id', $offer->id)
                     ->where('number', '>=', $offerp->number)
                     ->get();

        foreach ($data as $d) {
            $date = Carbon::createFromFormat('Y-m-d', $d->date);
            $d->date = $date->addMonths($months)->toDateString();
            $d->save();
        }

        return true;
    }

    public function cancel($offerp)
    {
        $offer = $offerp->offer;
        $data = $this->where('offer_id', $offer->id)
                     ->where('number', '>=', $offerp->number)
                     ->update(['status' => 'cancelado']);

        return true;
    }

    public function getCurrentOffer($offers)
    {
        $ids = [];
        foreach ($offers as $offer) {
            $ids[] = $offer->id;
        }
        $offer = $this->whereIn('offer_id', $ids)
                    ->whereYear('date', date('Y'))
                    ->whereMonth('date', date('m'))
                    ->first();

        return $offer;
    }

    public function getPayments($dt, $dt2, $type, $list, $rider)
    {
        $math = $dt2 ? '>=' : '=';
        $field = $type == 'pendente' ? 'date' : 'paid';
        $group = $type == 'pendente' ? 'date' : 'date_paid';
        $data = $this->with(['offer', 'offer.maintainer'])->whereDate($field, $math, $dt);
        if ($dt2) {
            $data = $data->whereDate($field, '<=', $dt2);
        }
        if ($rider) {
            $data = $data->whereHas('offer.maintainer', function ($query) use ($rider) {
                return $query->where('rider_id', $rider);
            });
        }
        if ($list) {
            $data = $data->whereHas('offer.maintainer', function ($query) use ($list) {
                return $query->where('list', $list);
            });
        }
        $data = $data->whereStatus($type)
                     ->get()
                     ->groupBy($group)
                     ->sortBy(function($item, $key) {
                        return $key;
                     });
        
        return $data;
    }

    public function getMaintainerAttribute()
    {
        $user = Maintainer::find($this->offer->maintainer_id);

        return $user;
    }

    public function getDatePaidAttribute()
    {
        return date('Y-m-d', strtotime($this->paid));
    }

    public function offer()
    {
        return $this->belongsTo(Offer::class, 'offer_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            return true;
        });
    }
}
