<?php

namespace App\Domains\Settings;

use App\Domains\Payments\Payment;
use Codecasts\Presenter\Presentable;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use Presentable;
    
    protected $presenter = SettingPresenter::class;
    
    public $timestamps = false;

    protected $fillable = [
        'type', 'value'
    ];
}
