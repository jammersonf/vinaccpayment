<?php

namespace App\Domains\OfferPaymentLogs;

use Illuminate\Database\Eloquent\Model;

class OfferPaymentLog extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'offer_payment_id', 'obs', 'date'
    ];

    public function offer()
    {
        return $this->belongsTo(OfferPayment::class, 'offer_payment_id');
    }

    public function scopeByPayment($query, $offerPayment)
    {
        return $query->where('offer_payment_id', $offerPayment);
    }

    public function newLog($id, $msg)
    {
        $this->create([
            'offer_payment_id' => $id,
            'obs' => $msg,
            'date' => date('Y-m-d')
        ]);

        return true;
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->date = date('Y-m-d H:i:s');
        });
    }
}
