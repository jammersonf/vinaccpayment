<?php

namespace App\Units\Offer;

use Codecasts\Support\Http\Routing\RouteFile;

class Routes extends RouteFile
{
    /**
     * Declare Web Routes.
     */
    public function routes()
    {
        $this->accessRoutes();
    }

    protected function accessRoutes()
    {
        $this->router->group(['prefix' => 'ofertaspap'], function () {
            $this->router->get('relatorio', 'OfferController@report')->name('offer.report');
            $this->router->post('relatorio', 'OfferController@report')->name('offer.report.search');
            $this->router->get('{id}/show', 'OfferController@show')
                         ->name('offer.show');
            $this->router->get('{maintainer}/create', 'OfferController@create')
                         ->name('offer.create');
            $this->router->get('{id}/delete', 'OfferController@delete')
                         ->name('offer.destroy');
        });

        $this->router->get('oferta/print/{id}', 'OfferController@printPc')
                     ->name('offer.print');
        $this->router->get('oferta/{id}', 'OfferController@printMobile');
    }
}
