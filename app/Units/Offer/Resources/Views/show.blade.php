@extends('core::layouts.app')

@section('title', 'Oferta > Pagamentos')

@section('page-actions')
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Pagamentos</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Mês</th>
                        <th>Oferta</th>
                        <th>Vencimento</th>
                        <th>Status</th>
                        <th>Data pagamento</th>
                        <th width="15%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($payments as $offer)
                        <tr>
                            <td>{{ $offer->id }}</td>
                            <td>{{ $offer->number }}</td>
                            <td>{{ $offer->value ? 
                                    $offer->present()->value : 
                                    number_format($maintainer->value, 2, ',','.') }}</td>
                            <td>
                                {!! Form::open(['method' => 'POST', 'route' => ['offerp.update', $offer->id]]) !!}
                                    {!! Form::date('date', $offer->date) !!}
                                    <button type="submit" class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                                {!! Form::close() !!}
                            </td>
                            <td>{{ $offer->status }}</td>
                            <td>
                                {!! Form::open(['method' => 'POST', 'route' => ['offerp.paid', $offer->id]]) !!}
                                    {!! Form::date('date', $offer->paid ? date('Y-m-d', strtotime($offer->paid)) : null) !!}
                                    <button type="submit" class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                                {!! Form::close() !!}
                            <td class="td-actions">
                                @if($offer->paid)
                                    <a href="{{route('offerp.reset', $offer->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-refresh"></i></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
        
    </div>
</div>
@stop
@section('scripts')
<script src="{{url('jquery.mask.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">

;(function($)
{
    'use strict';
    $(document).ready(function() {
    });
})(window.jQuery);
</script>
@stop
