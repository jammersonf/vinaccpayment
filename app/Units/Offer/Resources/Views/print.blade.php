Vinacc <br>
Rua Osvaldo Cruz, 229, Centenário.<br>
Campina Grande - PB<br>
CNPJ: 04625922/0001-45<br><br>
---- Recibo Pagamento ---<br>
Mantenedor: {{$offer->maintainer->name}}<br>
Agente: {{$offer->rider->name}}<br>
Data: {{$offerp->present()->paid}}<br>
@if($offerp->value)
Valor pago: {{$offerp->present()->value}}<br>
@endif
Parcela: {{$offerp->number}}<br><br>

<h4>Agradecemos a contribuição!</h4><br><br><br><br>
<a href="{{url('/motoqueiros/'.$offer->maintainer->rider_id.'/payment/'.$offer->maintainer->list.'#'.$offer->maintainer->neighborhood.$offer->maintainer->id)}}" class="btn btn-primary">Voltar</a>
