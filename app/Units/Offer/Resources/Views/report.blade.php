@extends('core::layouts.app')

@section('title', 'Ofertas PAP > Relatórios')

@section('page-actions')
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Filtros</h3>
            </div>
            <div class="panel-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    {!! Form::open(['route' => 'offer.report.search']) !!}
                        <div class="input-group">
                            {!! Form::select('rider', $riders, $rider, ['class' => 'form-control', 'style' => 'width: 170px;']) !!}
                            {!! Form::select('month', $monthOption, $month, ['class' => 'form-control', 'style' => 'width: 100px;']) !!}
                            <button class="btn btn-primary search" type="submit">Pesquisar</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @if($rider)
        <div class="panel panel-default">
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead class="primary">
                    <tr>
                        <th>Lista</th>
                        <th>Valor Total</th>
                        <th>Recebidos</th>
                        <th>%</th>
                        <th>Não Recebidos</th>
                        <th>%</th>
                        <th>Qtd</th>
                        <th>Qtd Pagos</th>
                        <th>Qtd Não Pagos</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($values as $key => $value)
                @if($key !== 'total')
                    <tr>
                        <td>{{ $key }}</td>
                        <td>R$ {{ number_format($value['paidValue'] + $value['waitingValue'], 2, ',', '.')}}</td>
                        <td>R$ {{ number_format($value['paidValue'], 2, ',', '.')}}</td>
                        <td>{{ $value['paidPercent']}}%</td>
                        <td>R$ {{ number_format($value['waitingValue'], 2, ',', '.')}}</td>
                        <td>{{ $value['waitingPercent']}}%</td>
                        <td>{{ $value['count'] }}</td>
                        <td>{{ $value['paid'] }}</td>
                        <td>{{ $value['waiting'] }}</td>
                    </tr>
                @endif
                @endforeach
                <tr class="bold">
                    <th>Total</th>
                    <th>R$ {{ number_format($values['total']['paidValue'] + $values['total']['waitingValue'], 2, ',', '.')}}</th>
                    <th>R$ {{ number_format($values['total']['paidValue'], 2, ',', '.')}}</th>
                    <th>{{ $values['total']['paidPercent']}}%</th>
                    <th>R$ {{ number_format($values['total']['waitingValue'], 2, ',', '.')}}</th>
                    <th>{{ $values['total']['waitingPercent']}}%</th>
                    <th>{{ $values['total']['count'] }}</th>
                    <th>{{ $values['total']['paid'] }}</th>
                    <th>{{ $values['total']['waiting'] }}</th>
                </tr>
            </table>
        </div>
        </div>
    @endif
</div>
@stop
