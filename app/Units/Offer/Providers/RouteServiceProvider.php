<?php

namespace App\Units\Offer\Providers;

use App\Units\Offer\Routes;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Units\Offer\Http\Controllers';

    public function boot()
    {
        parent::boot();
    }

    public function map()
    {
        (new Routes([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ]))->register();
    }
}
