<?php

namespace App\Units\Offer\Providers;

use Codecasts\Support\Units\ServiceProvider;

class UnitServiceProvider extends ServiceProvider
{
    protected $alias = 'offer';

    protected $hasViews = true;

    protected $hasTranslations = true;

    protected $providers = [
        EventServiceProvider::class,
        RouteServiceProvider::class,
    ];
}
