<?php

namespace App\Units\Offer\Http\Controllers;

use App\Domains\Maintainers\Maintainer;
use App\Domains\OfferPayments\OfferPayment;
use App\Domains\Offers\Offer;
use App\Domains\Users\User;
use Codecasts\Support\Http\Controller;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    protected $offer;
    protected $offerp;
    protected $user;

    public function __construct(Offer $offer, OfferPayment $op, User $u)
    {

        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        }
        $this->offer = $offer;
        $this->offerp = $op;
        $this->user = $u;
    }

    public function printPc($id)
    {
        $offerp = $this->offerp->find($id);
        $offer = $this->offer->find($offerp->offer_id);

        return view('offer::print', compact('offerp', 'offer'));
    }

    public function report(Request $request)
    {
        $input = $request->all();
        $values = [];
        $month = isset($input['month']) ? $input['month'] : date('m');
        $rider = isset($input['rider']) ? $input['rider']: '';
        $riders = $this->user->active()->rider()->pluck('name', 'id')->toArray();
        $monthOption = [
            '01' => 'Jan', '02' => 'Fev', '03' => 'Mar', '04'  => 'Abr',
            '05' => 'Mai', '06' => 'Jun', '07' => 'Jul', '08'  => 'Ago',
            '09' => 'Set', '10' => 'Out', '11' => 'Nov', '12' => 'Dez'
        ];

        if ($rider) {
            $user = $this->user->find($rider);
            $values = $user->getValuesByMonth($month);
        }
        return view('offer::report', compact('values', 'user', 'rider', 'month', 'riders', 'monthOption'));
    }

    public function printMobile($id)
    {
        $offerp = $this->offerp->find($id);
        $offer = $this->offer->find($offerp->offer_id);

        return view('offer::printmobile', compact('offerp', 'offer'));
    }

    public function show($id)
    {
        $offer = $this->offer->find($id);
        $payments = $offer->payments;
        $maintainer = Maintainer::find($offer->maintainer_id);
        
        return view('offer::show', compact('payments', 'maintainer'));
    }

    public function create(Maintainer $maintainer)
    {
        $this->offer->createPayments($maintainer);

        flash('Feito!');
        return redirect()->route('maintainer.panel', $maintainer->id);
    }

    public function delete($id)
    {
        $offer = $this->offer->find($id);
        $offer->delete();

        flash('Apagado!');
        return redirect()->route('maintainer.panel', $offer->maintainer_id);
    }

    public function toUsd($value)
    {
        $number = str_replace('.', '', $value);
        return str_replace(',', '.', $number);
    }
}
