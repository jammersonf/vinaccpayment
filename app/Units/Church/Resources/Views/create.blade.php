@extends('core::layouts.app')

@section('title', 'Igrejas > Novo')

@section('page-actions')
    <a href="{{ route('church.index') }}" class="btn btn-default">Voltar</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Entrada</h4>
                </div>

                <div class="panel-body">
                    {!! Form::open(['method' => 'post', 'route' => ['church.store']]) !!}

                        @include('church::form')

                        <button class="btn btn-primary btn-fill pull-right">Cadastrar</button>
                        <div class="clearfix"></div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@include('church::scripts')