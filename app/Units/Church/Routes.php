<?php

namespace App\Units\Church;

use Codecasts\Support\Http\Routing\RouteFile;

class Routes extends RouteFile
{
    /**
     * Declare Web Routes.
     */
    public function routes()
    {
        $this->accessRoutes();
    }

    protected function accessRoutes()
    {
        $this->router->group(['prefix' => 'igrejas'], function() {
            $this->router->get('/', 'ChurchController@index')->name('church.index');
            $this->router->get('cadastrar', 'ChurchController@create')->name('church.create');
            $this->router->post('cadastrar', 'ChurchController@store')->name('church.store');
            $this->router->post('pesquisar', 'ChurchController@search')->name('church.search');
            $this->router->get('{id}/editar', 'ChurchController@show')->name('church.show');
            $this->router->post('{id}/editar', 'ChurchController@update')->name('church.update');
            $this->router->get('{id}/deletar', 'ChurchController@destroy')->name('church.destroy');
        });
    }
}
