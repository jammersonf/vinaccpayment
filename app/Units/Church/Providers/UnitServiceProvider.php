<?php

namespace App\Units\Church\Providers;

use Codecasts\Support\Units\ServiceProvider;

class UnitServiceProvider extends ServiceProvider
{
    
    protected $alias = 'church';

    protected $hasViews = true;

    protected $hasTranslations = true;

    protected $providers = [
        EventServiceProvider::class,
        RouteServiceProvider::class,
    ];
}