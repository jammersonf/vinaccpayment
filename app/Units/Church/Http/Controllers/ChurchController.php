<?php

namespace App\Units\Church\Http\Controllers;

use App\Domains\Churches\Church;
use App\Domains\Churches\ChurchRepository;
use Codecasts\Support\Http\Controller;
use Illuminate\Http\Request;

class ChurchController extends Controller
{

    public function index()
    {
        $churchs = Church::orderId()->paginate();
        $render = true;

        return view('church::index', compact('churchs', 'render'));
    }

    public function create()
    {
        return view('church::create');
    }

    public function store(Request $request)
    {
        try {
            $input = $request->except('_token');
           
           Church::create($input);

            flash('Cadastrado com Sucesso', 'success');
        } catch (\Exception $e) {
            flash('Problema ao cadastrar Userro '.$e, 'danger');
            return redirect()->route('church.create');
        }

        return redirect()->route('church.index');
    }

    public function show($id)
    {
        $church = Church::find($id);

        return view('church::edit', compact('church'));
    }

    public function update($id, Request $request)
    {
        try {
            $input = $request->except('_token');
            
            Church::find($id)->update($input);

            flash('Atualizado com Sucesso', 'success');
        } catch (\Exception $e) {
            flash('Problema ao atualizar Userro '.$e, 'danger');
        }

        return redirect()->route('church.index');
    }

    public function destroy($id)
    {
        try {
            Church::find($id)->delete();

            flash("Apagado!", 'success');
        } catch (\Exception $e) {
            flash("problema ao apagar ".$e, 'danger');
        }

        return redirect()->route('church.index');
    }
}
