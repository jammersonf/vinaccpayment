<?php

namespace App\Units\Payment\Http\Controllers;

require_once base_path('vendor/pagarme/pagarme-php/Pagarme.php');

use App\Domains\Payments\Payment;
use App\Domains\Payments\PaymentRepository;
use App\Domains\Slips\Slip;
use App\Domains\Slips\SlipRepository;
use App\Domains\Users\User;
use App\Domains\Users\UserRepository;
use App\Units\Core\Jobs\SendMail;
use App\Units\Core\Jobs\SendSMS;
use App\Units\Payment\Events\PaymentStored;
use App\Units\Payment\Events\SingleStored;
use Codecasts\Support\Http\Controller;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    protected $slipRepository;
    protected $userRepository;

    public function __construct(SlipRepository $sr, UserRepository $ur)
    {
        $this->slipRepository = $sr;
        $this->userRepository = $ur;
    }

    public function index()
    {
        $payments = Payment::orderId()->paginate();
        $render = true;

        return view('payment::index', compact('payments', 'render'));
    }

    public function user($user)
    {
        $payments = Payment::with('user')->where('user_id', $user)->get();

        return view('payment::user', compact('payments', 'user'));
    }

    public function download($id)
    {
        $payment = Payment::find($id);

        return response()->download(public_path($payment->download));
    }

    public function create($user)
    {
        $user = User::find($user);
        return view('payment::create', compact('user'));
    }

    public function store($user, Request $request)
    {
        try {
            $input = $request->except('_token');
            $input['user_id'] = $user;
            $input['year'] = date('Y');
            $input['date'] = date('Y-m-d');
            $input['value'] = $this->toUsd($input['value']);

            $payment = Payment::create($input);
            $payment = Payment::find($payment->id);

            $customer = $this->userRepository->customer($user);
            
            $slips = $this->slipRepository->createSlips($payment, $customer, (int) $input['thisMonth']);
            
            $getUser = User::find($user);
            
            if (config('app.send')) {
                dispatch(new SendSMS(0, $getUser, 'carneenviado'));
                event(new PaymentStored($payment->id));
            }

            flash('Cadastrado com Sucesso', 'success');
            return redirect()->route('user.panel', $user);
        } catch (\Exception $e) {
            flash('Problema ao cadastrar Userro '.$e, 'danger');
            return redirect()->route('user.panel', $user);
        }
    }

    public function show($id)
    {
        $Payment = Payment::find($id);

        return view('payment::edit', compact('Payment'));
    }

    public function update($id, Request $request)
    {
        try {
            $input = $request->except('_token');
            
            Payment::find($id)->update($input);

            flash('Atualizado com Sucesso', 'success');
        } catch (\Exception $e) {
            flash('Problema ao atualizar Userro '.$e, 'danger');
        }

        return redirect()->route('payment.index');
    }

    public function destroy($id)
    {
        $payment = Payment::with(['slips', 'slips.logs'])->find($id);
        $payment->delete();

        flash("Apagado!", 'success');
        return redirect()->route('user.panel', $payment->user_id);
    }

    public function toUsd($value)
    {
        $number = str_replace('.', '', $value);
        return str_replace(',', '.', $number);
    }
}
