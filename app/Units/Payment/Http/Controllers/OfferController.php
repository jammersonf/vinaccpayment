<?php

namespace App\Units\Payment\Http\Controllers;

require_once base_path('vendor/pagarme/pagarme-php/Pagarme.php');

use App\Domains\Payments\Payment;
use App\Domains\Payments\PaymentRepository;
use App\Domains\Slips\Slip;
use App\Domains\Slips\SlipRepository;
use App\Domains\Users\User;
use App\Domains\Users\UserRepository;
use App\Units\Core\Jobs\SendMail;
use App\Units\Core\Jobs\SendSMS;
use App\Units\Payment\Events\PaymentStored;
use App\Units\Payment\Events\SingleStored;
use Codecasts\Support\Http\Controller;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    protected $slipRepository;
    protected $userRepository;

    public function __construct(SlipRepository $sr, UserRepository $ur)
    {
        $this->slipRepository = $sr;
        $this->userRepository = $ur;
    }
    
    public function create($user)
    {
        $user = User::find($user);
        $route = 'offer.store';
        $title = 'Vale Oferta';
        
        return view('payment::single', compact('user', 'route', 'title'));
    }

    public function store($user, Request $request)
    {
        try {
            $input = $request->except('_token');
            $input['value'] = $this->toUsd($input['value']);
            
            $customer = $this->userRepository->customer($user);

            $function = $input['date'] !== '' ? 'createSlipDate' : 'createSlipSingle';
            $slip = $this->slipRepository->$function($input, $user, $customer, 2);

            $getUser = User::find($user);
            
            if (config('app.send')) {
                dispatch(new SendSMS(0, $getUser, 'carneenviado'));
                event(new SingleStored($user, $input, 2));
            }

            flash('Cadastrado com Sucesso', 'success');
        } catch (\Exception $e) {
            flash('Problema ao cadastrar Userro '.$e, 'danger');
        }

        return redirect()->route('user.panel', $user);
    }

    public function toUsd($value)
    {
        $number = str_replace('.', '', $value);
        return str_replace(',', '.', $number);
    }
}
