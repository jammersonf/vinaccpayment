<?php

namespace App\Units\Payment\Http\Controllers;

require_once base_path('vendor/pagarme/pagarme-php/Pagarme.php');

use App\Domains\Payments\Payment;
use App\Domains\Payments\PaymentRepository;
use App\Domains\Slips\Slip;
use App\Domains\Slips\SlipRepository;
use App\Domains\Users\User;
use App\Domains\Users\UserRepository;
use App\Units\Core\Jobs\SendMail;
use App\Units\Core\Jobs\SendSMS;
use App\Units\Payment\Events\PaymentStored;
use App\Units\Payment\Events\SingleStored;
use Codecasts\Support\Http\Controller;
use Illuminate\Http\Request;

class SingleController extends Controller
{
    public function create($user)
    {
        $user = User::find($user);
        $route = 'single.store';
        $title = 'Avulso';

        return view('payment::single', compact('user', 'route', 'title'));
    }

    public function store($user, Request $request)
    {
        try {
            $input = $request->except('_token');
            $input['value'] = $this->toUsd($input['value']);
            
            $customer = $this->userRepository->customer($user);

            $function = $input['date'] !== '' ? 'createSlipDate' : 'createSlipSingle';
            $slip = $this->slipRepository->$function($input, $user, $customer, 1);

            $getUser = User::find($user);

            if (config('app.send')) {
                dispatch(new SendSMS(0, $getUser, 'carneenviado'));
                event(new SingleStored($user, $input, 1));
            }

            flash('Cadastrado com Sucesso', 'success');
        } catch (\Exception $e) {
            flash('Problema ao cadastrar Userro '.$e, 'danger');
        }

        return redirect()->route('user.panel', $user);
    }

    public function toUsd($value)
    {
        $number = str_replace('.', '', $value);
        return str_replace(',', '.', $number);
    }
}
