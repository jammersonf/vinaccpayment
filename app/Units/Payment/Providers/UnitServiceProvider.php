<?php

namespace App\Units\Payment\Providers;

use Codecasts\Support\Units\ServiceProvider;

class UnitServiceProvider extends ServiceProvider
{
    
    protected $alias = 'payment';

    protected $hasViews = true;

    protected $hasTranslations = true;

    protected $providers = [
        EventServiceProvider::class,
        RouteServiceProvider::class,
    ];
}