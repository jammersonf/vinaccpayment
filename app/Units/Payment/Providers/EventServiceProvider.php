<?php

namespace App\Units\Payment\Providers;

use App\Units\Payment\Events\PaymentStored;
use App\Units\Payment\Events\SingleStored;
use App\Units\Payment\Listeners\GeneratePdfCarne;
use App\Units\Payment\Listeners\GeneratePdfSingle;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        PaymentStored::class => [
            GeneratePdfCarne::class
        ],
        SingleStored::class => [
            GeneratePdfSingle::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
