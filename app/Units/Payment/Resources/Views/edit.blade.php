@extends('core::layouts.app')

@section('title', ''Usuário > Pagamentos > Alterar')

@section('page-actions')
    <a href="{{ route('payment.user', $user) }}" class="btn btn-default">Voltar</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Entrada</h4>
                </div>

                <div class="panel-body">
                    {!! Form::model($payment, ['method' => 'post', 'route' => ['payment.update', $payment->id]]) !!}
                    
                        @include('payment::form')

                        <button class="btn btn-primary btn-fill pull-right">Cadastrar</button>
                        <div class="clearfix"></div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@include('payment::scripts')