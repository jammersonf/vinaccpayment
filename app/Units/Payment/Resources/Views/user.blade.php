<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Carnês</h3>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Dia Pagamento</th>
                <th>Referente</th>
                <th>Oferta</th>
                <th width="15%">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @forelse($payments as $p)
                <tr>
                    <td>{{ $p->id }}</td>
                    <td>{{ $p->day }}</td>
                    <td>{{ $p->year }}</td>
                    <td>{{ $p->present()->value }}</td>
                    <td class="td-actions">
                        <a class="btn btn-primary btn-xs" title="ver boletos" href="{{ route('slip.show', $p->id)}}">
                            <i class="fa fa-list"></i>
                        </a>
                        <a class="btn btn-primary btn-xs" title="download do boleto" href="{{ route('payment.download', $p->id)}}">
                            <i class="fa fa-download"></i>
                        </a>
                        @if(auth()->user()->isAdmin())
                        <a class="btn btn-danger btn-xs" title="Apagar boleto" href="{{ route('payment.remove', $p->id)}}">
                            <i class="fa fa-remove"></i>
                        </a>
                        @endif
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="3" class="text-center">Vazio</td>
                </tr>
            @endforelse
        </tbody>
    </table>
    </div>
</div>