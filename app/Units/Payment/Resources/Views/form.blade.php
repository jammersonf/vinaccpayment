<div class="row">
    <div class="col-xs-6 col-md-2">
        {!! Form::label('day', 'Dia pagamento')!!}
        {!! Form::number('day', null, ['class' => 'form-control', 'required']) !!}
    </div>
    <div class="col-xs-6 col-md-2">
        {!! Form::label('value', 'Valor da orferta')!!}
        {!! Form::text('value', null, ['class' => 'form-control valor', 'required']) !!}
    </div>
    <div class="col-xs-6 col-md-2">
        {!! Form::label('thisMonth', 'Gerar para o mês atual')!!}
        {!! Form::radio('thisMonth', 0) !!} Não | {!! Form::radio('thisMonth', 1, true) !!} Sim
    </div>
</div>