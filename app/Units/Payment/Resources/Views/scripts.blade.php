@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
;(function($)
{
    'use strict';

    $(document).ready(function() {
    	$(".valor").maskMoney({thousands:'.', decimal:',', affixesStay: false});
    });
})(window.jQuery);
</script>
@endsection