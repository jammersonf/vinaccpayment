@extends('core::layouts.app')

@section('title', 'Mantenedor > Carnê > Novo')

@section('page-actions')
    <a href="{{ route('user.panel', $user->id) }}" class="btn btn-default">Voltar</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">{{$user->name}}</h4>
                </div>

                <div class="panel-body">
                    {!! Form::open(['method' => 'post', 'route' => ['payment.store', $user->id]]) !!}

                        @include('payment::form')

                        <button class="btn btn-primary btn-fill pull-right">Cadastrar</button>
                        <div class="clearfix"></div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@include('payment::scripts')