@extends('core::layouts.app')

@section('title', 'Mantenedor > Boletos > '.$title)

@section('page-actions')
    <a href="{{ route('user.panel', $user->id) }}" class="btn btn-default">Voltar</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">{{$user->name}}</h4>
                </div>

                <div class="panel-body">
                    {!! Form::open(['method' => 'post', 'route' => [$route, $user->id]]) !!}
                        <div class="row">
                            <div class="col-xs-6 col-md-3">
                                {!! Form::label('date', 'Data')!!}
                                {!! Form::date('date', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-xs-6 col-md-2">
                                {!! Form::label('day', 'Dia pagamento')!!}
                                {!! Form::number('day', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-xs-4 col-md-2">
                                {!! Form::label('value', 'Valor da orferta')!!}
                                {!! Form::text('value', null, ['class' => 'form-control valor', 'required']) !!}
                            </div>
                            <div class="col-xs-4 col-md-2">
                                {!! Form::label('qtd', 'Qtd')!!}
                                {!! Form::number('qtd', 1, ['class' => 'form-control', 'required']) !!}
                            </div>
                            <div class="col-xs-6 col-md-2">
                                {!! Form::label('thisMonth', 'Gerar para o mês atual')!!}
                                {!! Form::radio('thisMonth', 0) !!} Não | {!! Form::radio('thisMonth', 1, true) !!} Sim
                            </div>
                        </div>
                        <button class="btn btn-primary btn-fill pull-right">Cadastrar</button>
                        <div class="clearfix"></div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@include('payment::scripts')