<?php

namespace App\Units\Payment\Events;

use App\Domains\Bets\Bet;
use Illuminate\Queue\SerializesModels;

class PaymentStored
{
    use SerializesModels;

    public $payment;

    public function __construct($payment)
    {
        $this->payment = $payment;
    }

}
