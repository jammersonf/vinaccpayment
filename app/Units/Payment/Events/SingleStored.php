<?php

namespace App\Units\Payment\Events;

use Illuminate\Queue\SerializesModels;

class SingleStored
{
    use SerializesModels;

    public $user;
    public $input;
    public $single;

    public function __construct($user, $input, $single)
    {
        $this->user = $user;
        $this->input = $input;
        $this->single = $single;
    }
}
