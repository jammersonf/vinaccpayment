<?php

namespace App\Units\Payment\Listeners;

use App\Domains\Payments\Payment;
use App\Domains\Settings\Setting;
use App\Domains\SlipLogs\SlipLog;
use App\Domains\Slips\Slip;
use App\Domains\Users\User;
use App\Units\Payment\Events\SingleStored;
use Illuminate\Contracts\Queue\ShouldQueue;

class GeneratePdfSingle implements ShouldQueue
{
    public function handle(SingleStored $event)
    {
        $user = User::find($event->user);
        $html = $this->setHtmlSlip($event->user, $event->single);

        $this->sendMail($user, $html);
    }

    public function setHtmlSlip($user, $single)
    {
        $slips = Slip::where('single', $single)
                     ->where('user_id', $user)
                     ->whereNull('datePaid')
                     ->get();
        $html = '';
        foreach ($slips as $slip) {
            $html .= " <br> <a href='".$slip->boleto_url."'>Clique para baixar o boleto ". $slip->slip ." com vencimento para ".date('d/m/Y', strtotime($slip->date))."</a>";
        }

        return $html;
    }

    public function sendMail($user, $slips)
    {
        $msg = $this->getMsg('carneenviado');
        $params = $this->getArr($user, $msg, $slips);

        \Guzzle::post('https://www51.e-goi.com/api/public/mail/send', [ \GuzzleHttp\RequestOptions::JSON => $params]);

        return true;
    }

    public function getMsg($type)
    {
        return Setting::where('type', $type)->first();
    }

    public function getArr($user, $msg, $html)
    {
        return [
            'subject' => 'VINACC',
            'email' => $user->email,
            'senderHash' => 'ceda28f4f2b99a4b81220a90af9586c3',
            /*'message' => [
                'html' => $msg->value
            ],*/
            'message' => [
                'html' => $msg->value.$html
            ],
            'openTrackingEnabled' => true,
            'clickTrackingEnabled' => true,
            'priority' => 'non-urgent',
            /*'attachments' => [
                [
                    'filename' => 'Carne-'.$payment->id.'.pdf',
                    'data' => chunk_split(base64_encode(file_get_contents($payment->download))),
                    'disposition' => 'attachment'
                ]
            ],*/
            'apikey' => config('api.egoi.key')
        ];
    }
}
