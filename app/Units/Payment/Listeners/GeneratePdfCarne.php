<?php

namespace App\Units\Payment\Listeners;

use App\Domains\Payments\Payment;
use App\Domains\Settings\Setting;
use App\Domains\SlipLogs\SlipLog;
use App\Domains\Slips\Slip;
use App\Units\Payment\Events\PaymentStored;
use App\Units\Slip\Services\SlipService;
use Illuminate\Contracts\Queue\ShouldQueue;

class GeneratePdfCarne implements ShouldQueue
{
    public function __construct()
    {
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        }
    }

    public function handle(PaymentStored $ps)
    {
        $payment = $ps->payment;
        $slipService = new SlipService();
        $slips = Slip::where('payment_id', $payment)->get();
        //$pdfMerge = new \LynX39\LaraPdfMerger\PdfManage();
        $render = '';
        foreach ($slips as $slip) {
            //juntar view pro carne
            $data = $slipService->mount($slip->boleto_url);
            $render .= view('slip::boletoraw', compact('data'))->render();

            //gera boleto individual
            $pdf = \App::make('snappy.pdf');
            $name = 'pdf/'.$payment.'/'.$slip->id.'.pdf';
            $pdf->generateFromHtml(file_get_contents($slip->boleto_url), public_path($name), ['encoding' => 'UTF-8']);
            //$pdfMerge->addPDF($name);

            $slip->download = $name;
            $slip->save();
        }
        $url = 'pdf/'.$payment.'/carne-'.$payment.'.pdf';
        $pdf->generateFromHtml($render, public_path($url), ['encoding' => 'UTF-8']);
        //$pdfMerge->merge('file', public_path($url), 'P');

        $paymentModel = Payment::find($payment)->update(['download' => $url]);

        $this->sendMail($payment);
    }

    public function sendMail($payment)
    {
        $payment = Payment::with('user')->find($payment);
        $msg = $this->getMsg('carneenviado');
        $params = $this->getArr($payment, $msg);

        \Guzzle::post('https://www51.e-goi.com/api/public/mail/send', [ \GuzzleHttp\RequestOptions::JSON => $params]);

        return true;
    }

    public function getMsg($type)
    {
        return Setting::where('type', $type)->first();
    }

    public function getArr($payment, $msg)
    {
        return [
            'subject' => 'VINACC',
            'email' => $payment->user->email,
            'senderHash' => 'ceda28f4f2b99a4b81220a90af9586c3',
            /*'message' => [
                'html' => $msg->value
            ],*/
            'message' => [
                'html' => $msg->value." <br> <a href='".route('payment.download', $payment->id)."'>Clique para baixar o carnê com os boletos</a>"
            ],
            'openTrackingEnabled' => true,
            'clickTrackingEnabled' => true,
            'priority' => 'non-urgent',
            /*'attachments' => [
                [
                    'filename' => 'Carne-'.$payment->id.'.pdf',
                    'data' => chunk_split(base64_encode(file_get_contents($payment->download))),
                    'disposition' => 'attachment'
                ]
            ],*/
            'apikey' => config('api.egoi.key')
        ];
    }
}
