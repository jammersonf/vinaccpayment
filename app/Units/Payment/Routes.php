<?php

namespace App\Units\Payment;

use Codecasts\Support\Http\Routing\RouteFile;

class Routes extends RouteFile
{
    /**
     * Declare Web Routes.
     */
    public function routes()
    {
        $this->accessRoutes();
    }

    protected function accessRoutes()
    {
        $this->router->group(['prefix' => 'pagamentos'], function () {
            $this->router->get('/', 'PaymentController@index')->name('payment.index');
            $this->router->post('pesquisar', 'PaymentController@search')->name('payment.search');
            $this->router->get('{id}/editar', 'PaymentController@show')->name('payment.show');
            $this->router->get('{id}/apagar', 'PaymentController@destroy')->name('payment.remove');
            $this->router->post('{id}/editar', 'PaymentController@update')->name('payment.update');
            $this->router->get('{id}/deletar', 'PaymentController@destroy')->name('payment.destroy');
            $this->router->get('{id}/download', 'PaymentController@download')->name('payment.download');

            $this->router->group(['prefix' => 'carne'], function () {
                $this->router->get('{user}/cadastrar', 'PaymentController@create')->name('payment.create');
                $this->router->post('{user}/cadastrar', 'PaymentController@store')->name('payment.store');
            });

            $this->router->group(['prefix' => 'avulso'], function () {
                $this->router->get('{user}/cadastrar', 'SingleController@create')->name('single.create');
                $this->router->post('{user}/cadastrar', 'SingleController@store')->name('single.store');
            });

            $this->router->group(['prefix' => 'valeoferta'], function () {
                $this->router->get('{user}/cadastrar', 'OfferController@create')->name('offer.vale.create');
                $this->router->post('{user}/cadastrar', 'OfferController@store')->name('offer.store');
            });
        });
    }
}
