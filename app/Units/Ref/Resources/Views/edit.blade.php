@extends('core::layouts.app')

@section('title', 'Origens > Alterar')

@section('page-actions')
    <a href="{{ route('ref.index') }}" class="btn btn-default">Voltar</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Entrada</h4>
                </div>

                <div class="panel-body">
                    {!! Form::model($ref, ['method' => 'post', 'route' => ['ref.update', $ref->id]]) !!}
                    
                        @include('ref::form')

                        <button class="btn btn-primary btn-fill pull-right">Cadastrar</button>
                        <div class="clearfix"></div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@include('ref::scripts')