@extends('core::layouts.app')

@section('title', 'Origens')

@section('page-actions')
    <a href="{{ route('ref.create') }}" class="btn btn-fill btn-primary">Cadastrar</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Listagem</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th width="15%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($refs as $ref)
                            <tr>
                                <td>{{ $ref->id }}</td>
                                <td>{{ $ref->name }}</td>
                                <td class="td-actions">
                                    <a class="btn btn-primary btn-xs" href="{{ route('ref.show', $ref->id)}}">
                                        <i class="fa fa-pencil"></i>
                                    </a>   
                                    <a class="btn btn-danger btn-xs delete" href="{{ route('ref.destroy', $ref->id) }}">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">Vazio</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                @if($render)
                    {{ $refs->links() }}
                @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
;(function($)
{
    'use strict';

    $(document).ready(function() {
        $(".delete").click(function () {
            var r = confirm("Deseja mesmo excluir?");
            if (! r) {
                return false;
            }
        });
    });
})(window.jQuery);
</script>
@endsection