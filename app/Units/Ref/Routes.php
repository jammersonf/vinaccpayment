<?php

namespace App\Units\Ref;

use Codecasts\Support\Http\Routing\RouteFile;

class Routes extends RouteFile
{
    /**
     * Declare Web Routes.
     */
    public function routes()
    {
        $this->accessRoutes();
    }

    protected function accessRoutes()
    {
        $this->router->group(['prefix' => 'origens'], function() {
            $this->router->get('/', 'RefController@index')->name('ref.index');
            $this->router->get('cadastrar', 'RefController@create')->name('ref.create');
            $this->router->post('cadastrar', 'RefController@store')->name('ref.store');
            $this->router->post('pesquisar', 'RefController@search')->name('ref.search');
            $this->router->get('{id}/editar', 'RefController@show')->name('ref.show');
            $this->router->post('{id}/editar', 'RefController@update')->name('ref.update');
            $this->router->get('{id}/deletar', 'RefController@destroy')->name('ref.destroy');
        });
    }
}
