<?php

namespace App\Units\Ref\Http\Controllers;

use App\Domains\Refs\Ref;
use App\Domains\Refs\RefRepository;
use Codecasts\Support\Http\Controller;
use Illuminate\Http\Request;

class RefController extends Controller
{

    public function index()
    {
        $refs = Ref::orderId()->paginate();
        $render = true;

        return view('ref::index', compact('refs', 'render'));
    }

    public function create()
    {
        return view('ref::create');
    }

    public function store(Request $request)
    {
        try {
            $input = $request->except('_token');
            
            Ref::create($input);

            flash('Cadastrado com Sucesso', 'success');
        } catch (\Exception $e) {
            flash('Problema ao cadastrar Userro '.$e, 'danger');
            return redirect()->route('ref.create');
        }

        return redirect()->route('ref.index');
    }

    public function show($id)
    {
        $ref = Ref::find($id);

        return view('ref::edit', compact('ref'));
    }

    public function update($id, Request $request)
    {
        try {
            $input = $request->except('_token');
            
            Ref::find($id)->update($input);

            flash('Atualizado com Sucesso', 'success');
        } catch (\Exception $e) {
            flash('Problema ao atualizar Userro '.$e, 'danger');
        }

        return redirect()->route('ref.index');
    }

    public function destroy($id)
    {
        try {
            Ref::find($id)->delete();

            flash("Apagado!", 'success');
        } catch (\Exception $e) {
            flash("problema ao apagar ".$e, 'danger');
        }

        return redirect()->route('ref.index');
    }
}
