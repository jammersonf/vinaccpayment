@extends('core::layouts.app')

@section('title', 'Mantenedores')

@section('page-actions')
    <a href="{{ route('maintainer.create') }}" class="btn btn-fill btn-primary">Cadastrar</a>
    <a href="{{ route('maintainer.blockeds') }}" class="btn btn-lock btn-primary">Bloqueados</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Filtros</h3>
                            </div>
                            <div class="panel-body">
                            {!! Form::open(['route' => 'maintainer.search']) !!}
                                <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                                    <input type="text" name="usu" class="form-control date" placeholder="Nome ou Código" value="{{$usu or ''}}" />
                                </div>
                                <div class="col-xs-3 col-md-3">
                                    {!! Form::select('rider', $riders, null, ['class' => 'form-control', 'placeholder' => 'Motoqueiro']) !!}
                                </div>
                                <div class="col-xs-2 col-md-2">
                                    {!! Form::select('list', $list, null, ['class' => 'form-control', 'placeholder' => 'Lista']) !!}
                                </div>
                                    <button class="btn btn-default btn-sm search" type="submit">Pesquisar</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Total: {{ $count }}</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Telefone</th>
                            <th>Parcela</th>
                            <th>Lista</th>
                            <th>Valor</th>
                            <th>Motoqueiro</th>
                            <th width="15%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($users as $maintainer)
                            <tr>
                                <td>{{ $maintainer->id }}</td>
                                <td>{{ $maintainer->name }}</td>
                                <td>{{ $maintainer->phone }}</td>
                                <td>{{ $maintainer->offer() ? $maintainer->offer()->number : '' }}</td>
                                <td>{{ $maintainer->list }}</td>
                                <td>{{ number_format($maintainer->value, 2, ',', '.') }}</td>
                                <td>{{ $maintainer->rider->name }}</td>
                                <td class="td-actions">
                                    <a class="btn btn-primary btn-xs" title="editar informações" href="{{ route('maintainer.show', $maintainer->id)}}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a class="btn btn-primary btn-xs" title="Ver painel do usuário" href="{{ route('maintainer.panel', $maintainer->id)}}">
                                        <i class="fa fa-newspaper-o"></i>
                                    </a>
                                    @php
                                        $status = $maintainer->status == 1 ? 0 : 1;
                                        $class = $status == 0 ? 'danger' : 'success';
                                        $icon = $status == 0 ? 'lock' : 'unlock';
                                    @endphp
                                    <a class="btn btn-{{$class}} btn-xs delete" href="{{ route('maintainer.status', [$maintainer->id, $status]) }}">
                                        <i class="fa fa-{{$icon}}"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">Vazio</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                </div>
            </div>
            @if(isset($paginate) && $paginate)
                {{ $users->links() }}
            @endif
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
;(function($)
{
    'use strict';

    $(document).ready(function() {
        $(".delete").click(function () {
            var r = confirm("Deseja mesmo executar essa ação?");
            if (! r) {
                return false;
            }
        });
    });
})(window.jQuery);
</script>
@endsection
