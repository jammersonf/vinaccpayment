@extends('core::layouts.app')

@section('title', 'Pendências')

@section('page-actions')

@endsection

@section('content')
    <style>
        @media  print{a[href]:after{content:none}}
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Total de parcelas atrasadas: {{ $count }}</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Telefone</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($arr as $user)
                            <tr>
                                <td>{{ $user[0]->user->id }}</td>
                                <td><a href="{{ route('user.panel', $user[0]->user->id)}}">{{ $user[0]->user->name }}</a></td>
                                <td>{{ $user[0]->user->phone }}</td>
                                <td>{{ $user[0]->user->email }}</td>
                            </tr>
                            @foreach($user as $slip)
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="2"><b>Vencimento: {{ $slip->present()->date }} -
                                Parcela: {{ $slip->slip }} - 
                                Valor: R${{ $slip->present()->value }}</b></td>
                            </tr>
                            @endforeach
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">Vazio</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
;(function($)
{
    'use strict';

    $(document).ready(function() {
        $(".delete").click(function () {
            var r = confirm("Deseja mesmo executar essa ação?");
            if (! r) {
                return false;
            }
        });
    });
})(window.jQuery);
</script>
@endsection