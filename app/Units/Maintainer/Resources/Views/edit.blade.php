@extends('core::layouts.app')

@section('title', 'Cadastro > Alterar')

@section('page-actions')
    <a href="{{ route('maintainer.index') }}" class="btn btn-default">Voltar</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">{{ $maintainer->name }}</h4>
                </div>

                <div class="panel-body">
                    {!! Form::model($maintainer, ['method' => 'post', 'route' => ['maintainer.update', $maintainer->id]]) !!}
                    
                        @include('maintainer::form')

                        <button class="btn btn-primary btn-fill pull-right">Update</button>
                        <div class="clearfix"></div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@include('maintainer::scripts')