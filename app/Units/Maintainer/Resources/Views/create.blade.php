@extends('core::layouts.app')

@section('title', 'Cadastro > Novo')

@section('page-actions')
    <a href="{{ route('maintainer.index') }}" class="btn btn-default">Voltar</a>
@endsection

@section('content')
    @if( ! auth()->check())
        <h3>Insira seus Dados</h3>
        <br><br>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Dados</h4>
                </div>

                <div class="panel-body">
                    {!! Form::open(['method' => 'post', 'route' => ['maintainer.store']]) !!}

                        @include('maintainer::form')

                        <button class="btn btn-primary btn-fill pull-right">Cadastrar</button>
                        <div class="clearfix"></div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@include('maintainer::scripts')