@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/jquery-mask-plugin@1.14.15/dist/jquery.mask.min.js"></script>
<script type="text/javascript">
;(function($)
{
    'use strict';

    $(document).ready(function() {
        $(".cep").mask("99999-999");
        $('.cpf').mask('999.999.999-99', {reverse: true});
        $('.phone').mask('(99) 99999-9999');
        $('.cep').val('58400-000');
        $(".cidade").val('Campina Grande');
        $(".estado").val('PB');
    	$(".cep").blur(function() {
            var cep = $.trim($(this).val());
                var url = 'https://viacep.com.br/ws/'+cep+'/json/';
                $.get(url,{cep:cep},
                    function (rs) {
                        //rs = $.parseJSON(rs);
                        if(rs != 0){
                            $(".endereco").val(rs.logradouro);
                            $(".bairro").val(rs.bairro);
                            $(".cidade").val(rs.localidade);
                            $(".estado").val(rs.uf);
                        }
                    })
        });
        $(".doc").change(function() {
            var doc = $(this).val();
            if (doc == 'cpf') {
                $('.cpf').mask('999.999.999-99', {reverse: true});
            } else {
                $('.cpf').mask('99.999.999/9999-99', {reverse: true});
            }
        });
    });
})(window.jQuery);
</script>
@endsection
