<div class="row">
    <div class="col-xs-6 col-md-4">
        {!! Form::label('name', 'Nome')!!}
        {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
    </div>
    <div class="col-xs-4 col-md-3">
        {!! Form::label('email', 'Email') !!}
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-xs-4 col-md-2">
        {!! Form::label('documento', 'Tipo do documento') !!}<br>
        {!! Form::radio('documento', 'cpf', true, ['class' => 'doc']) !!} CPF |
        {!! Form::radio('documento', 'cnpj', false, ['class' => 'doc']) !!} CNPJ
    </div>
    <div class="col-xs-4 col-md-3">
        {!! Form::label('cpf', 'Documento') !!}
        {!! Form::text('cpf', null, ['class' => 'form-control cpf']) !!}
    </div>
    <div class="col-xs-4 col-md-3">
        {!! Form::label('phone', 'Telefone') !!}
        {!! Form::text('phone', null, ['class' => 'form-control phone']) !!}
    </div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
        {!! Form::label('cep', 'CEP*')!!}
        {!! Form::text('cep', null, ['class' => 'form-control cep', 'placeholder' => 'CEP']) !!}
    </div>
    <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
        {!! Form::label('street', 'Endereço*')!!}
        {!! Form::text('street', null, ['class' => 'form-control endereco', 'placeholder' => 'Endereço']) !!}
    </div>
    <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
        {!! Form::label('neighborhood', 'Bairro*')!!}
        {!! Form::text('neighborhood', null, ['class' => 'form-control bairro', 'placeholder' => 'Bairro']) !!}
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
        {!! Form::label('city', 'Cidade*')!!}
        {!! Form::text('city', null, ['class' => 'form-control cidade', 'placeholder' => 'Cidade']) !!}
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
        {!! Form::label('uf', 'Estado*')!!}
        {!! Form::text('uf', null, ['class' => 'form-control estado', 'placeholder' => 'Estado']) !!}
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
        {!! Form::label('number', 'Número')!!}
        {!! Form::text('number', null, ['class' => 'form-control numero', 'placeholder' => 'Num']) !!}
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
        {!! Form::label('complemento', 'Complemento')!!}
        {!! Form::text('complemento', null, ['class' => 'form-control numero']) !!}
    </div>
    <div class="col-xs-3 col-md-3">
        {!! Form::label('created_at', 'Início contrato') !!}
        {!! Form::date('created_at', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-xs-4 col-md-3">
        {!! Form::label('nascimento', 'Data de nascimento') !!}
        {!! Form::date('nascimento', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-xs-3 col-md-3">
        {!! Form::label('rider_id', 'Motoqueiro') !!}
        {!! Form::select('rider_id', $riders, null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-xs-2 col-md-2">
        {!! Form::label('list', 'Lista') !!}
        {!! Form::select('list', $list, null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-xs-2 col-md-2">
        {!! Form::label('value', 'Valor') !!}
        {!! Form::number('value', null, ['class' => 'form-control', 'step' => 'any']) !!}
    </div>
    <div class="col-xs-4 col-md-3">
        {!! Form::label('obs', 'Observação') !!}
        {!! Form::text('obs', null, ['class' => 'form-control']) !!}
    </div>
</div>
