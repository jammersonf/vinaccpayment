@extends('core::layouts.app')

@section('title', 'Painel')

@section('page-actions')
    <a href="{{route('offer.create', $maintainer->id)}}" class="btn btn-primary"><i class="fa fa-save"></i> Criar Pagamento</a>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-xs-4 col-md-4">Nome: <b>{{$maintainer->name}}</b></div>
            <div class="col-xs-4 col-md-4">Telefone: <b>{{$maintainer->phone}}</b></div>
            <div class="col-xs-4 col-md-4">Endereço: <b>{{$maintainer->street}}, {{$maintainer->number}}</b></div>
            @if ($maintainer->rider_id)
                <div class="col-xs-4 col-md-4">Motoqueiro: <b>{{$maintainer->rider->name }}</b></div>
            @endif
        </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Oferta do mês</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Parcela</th>
                        <th>Oferta</th>
                        <th>Vencimento</th>
                        <th>Status</th>
                        <th>Data pagamento</th>
                        <th width="15%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @if($offer)
                        <tr>
                            <td>{{ $offer->id }}</td>
                            <td>{{ $offer->number }}</td>
                            <td>{{ $offer->value ? 
                                        $offer->present()->value : 
                                        number_format($maintainer->value, 2, ',','.') }}</td>
                            <td>{{ $offer->present()->date }}</td>
                            <td>{{ $offer->status }}</td>
                            <td>{{ $offer->present()->paid }}</td>
                            <td class="td-actions">
                            </td>
                        </tr>
                    @else
                        <tr>
                            <td colspan="3" class="text-center">Sem oferta para o mês atual</td>
                        </tr>
                    @endif
                </tbody>
            </table>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Ofertas</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Oferta</th>
                        <th>Ano</th>
                        <th>Lista</th>
                        <th width="15%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($offers as $offer)
                        <tr>
                            <td>{{ $offer->id }}</td>
                            <td>{{ number_format($maintainer->value, 2, ',','.') }}</td>
                            <td>{{ $offer->year }}</td>
                            <td>{{ $offer->list }}</td>
                            <td class="td-actions">
                                <a href="{{route('offer.show', $offer->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>
                                @if(auth()->user()->isAdmin())
                                    <a class="btn btn-danger btn-xs" title="Apagar boleto" href="{{ route('offer.destroy', $offer->id)}}">
                                        <i class="fa fa-remove"></i>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
        
    </div>
</div>
@stop
@section('scripts')
<script src="{{url('jquery.mask.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">

;(function($)
{
    'use strict';
    $(document).ready(function() {
    });
})(window.jQuery);
</script>
@stop
