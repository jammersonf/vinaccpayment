<?php

namespace App\Units\Maintainer\Http\Controllers;

use App\Domains\Maintainers\Maintainer;
use App\Domains\OfferPayments\OfferPayment;
use App\Domains\Offers\Offer;
use App\Domains\Users\User;
use Codecasts\Support\Http\Controller;
use Illuminate\Http\Request;

class MaintainerController extends Controller
{
    protected $maintainer;
    protected $offer;

    public function __construct(Maintainer $user, Offer $offer)
    {
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        }
        $this->maintainer = $user;
        $this->offer = $offer;
    }

    public function index()
    {
        $users = $this->maintainer->orderBy('id', 'DESC')->active()->paginate();
        $count = $this->maintainer->active()->count();
        $riders = User::active()->rider()->pluck('name', 'id')->toArray();
        $list = [5 => 5, 10 => 10, 15 => 15, 20 => 20, 25 => 25, 30 => 30];
        $paginate = true;
        return view('maintainer::index', compact('users', 'count', 'riders', 'list', 'paginate'));
    }

    public function blockeds()
    {
        $users = $this->maintainer->orderBy('id', 'DESC')->blocked()->paginate();
        $count = $this->maintainer->blocked()->count();
        $riders = User::active()->rider()->pluck('name', 'id')->toArray();
        $list = [5 => 5, 10 => 10, 15 => 15, 20 => 20, 25 => 25, 30 => 30];
        $paginate = true;
        return view('maintainer::index', compact('users', 'count', 'riders', 'list', 'paginate'));
    }

    public function renews()
    {
        $users = $this->maintainer->active()
                                  ->get();
        
        return view('maintainer::renews', compact('users'));
    }

    public function status($id, $status)
    {
        $maintainer = $this->maintainer->find($id);
        $maintainer->status = $status;
        $maintainer->save();

        return redirect()->route('maintainer.index');
    }

    public function panel($id)
    {
        $maintainer = $this->maintainer->find($id);
        $offers = $maintainer->offers;
        $offer = (new OfferPayment())->getCurrentOffer($offers);
        
        return view('maintainer::dashboard', compact('maintainer', 'offer', 'offers'));
    }

    public function search(Request $input)
    {
        $maintainer = $this->maintainer->active();
        if ($input->usu) {
            $maintainer = $maintainer->where('name', 'like', '%'.$input->usu.'%');
        }
        if ($input->rider) {
            $maintainer = $maintainer->where('rider_id', $input->rider);
        }
        if ($input->list) {
            $maintainer = $maintainer->whereList($input->list);
        }
        
        $users = $maintainer->get();
        $count = $maintainer->count();
        $riders = User::active()->rider()->pluck('name', 'id')->toArray();
        $list = [5 => 5, 10 => 10, 15 => 15, 20 => 20, 25 => 25, 30 => 30];
        $paginate = false;

        return view('maintainer::index', compact('users', 'count', 'riders', 'list', 'paginate'));
    }

    public function create()
    {
        $riders = User::active()->rider()->pluck('name', 'id')->toArray();
        $list = [5 => 5, 10 => 10, 15 => 15, 20 => 20, 25 => 25, 30 => 30];

        return view('maintainer::create', compact('riders', 'list'));
    }

    public function show($id)
    {
        $maintainer = $this->maintainer->find($id);
        $riders = User::active()->rider()->pluck('name', 'id')->toArray();
        $list = [5 => 5, 10 => 10, 15 => 15, 20 => 20, 25 => 25, 30 => 30];

        return view('maintainer::edit', compact('maintainer', 'riders', 'list'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['phone'] = str_replace(['(', ')', ' ', '-'], '', $input['phone']);

        /*$duplicity = $this->maintainer->checkDuplicity($input);
        if ($duplicity) {
            $msg = ['Usuário já cadastrado', 'warning'];
            $route = 'maintainer.create';
        } else {*/
        $msg = ['Usuário cadastrado!', 'success'];
        $route = 'maintainer.index';
        $maintainer = $this->maintainer->create($input);
        $this->offer->createPayments($maintainer);
        //}

        flash($msg[0], $msg[1]);
        return redirect()->route($route);
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        $input['phone'] = str_replace(['(', ')', ' ', '-'], '', $input['phone']);
        $maintainer = $this->maintainer->find($id);
        $maintainer->update($input);
        $maintainer->offers()->update(['list' => $input['list']]);

        flash('Alterações salvas!');
        return redirect()->route('maintainer.index');
    }

    public function toUsd($value)
    {
        $number = str_replace('.', '', $value);
        return str_replace(',', '.', $number);
    }
}
