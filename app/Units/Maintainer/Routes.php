<?php

namespace App\Units\Maintainer;

use Codecasts\Support\Http\Routing\RouteFile;

class Routes extends RouteFile
{
    /**
     * Declare Web Routes.
     */
    public function routes()
    {
        $this->accessRoutes();
    }

    protected function accessRoutes()
    {
        $this->router->group(['prefix' => 'mantenedorespap'], function () {
            $this->router->get('/', 'MaintainerController@index')->name('maintainer.index');
            $this->router->get('blockeds', 'MaintainerController@blockeds')->name('maintainer.blockeds');
            $this->router->post('/', 'MaintainerController@search')->name('maintainer.search');
            $this->router->get('create', 'MaintainerController@create')->name('maintainer.create');
            $this->router->post('create', 'MaintainerController@store')->name('maintainer.store');
            $this->router->get('{id}/edit', 'MaintainerController@show')->name('maintainer.show');
            $this->router->get('{id}/status/{status}', 'MaintainerController@status')->name('maintainer.status');
            $this->router->post('{id}/edit', 'MaintainerController@update')->name('maintainer.update');
            $this->router->get('{id}/painel', 'MaintainerController@panel')->name('maintainer.panel');
        });
    }
}
