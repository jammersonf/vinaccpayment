<?php

namespace App\Units\Maintainer\Providers;

use App\Units\Maintainer\Routes;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Units\Maintainer\Http\Controllers';

    public function boot()
    {
        parent::boot();
    }

    public function map()
    {
        (new Routes([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ]))->register();
    }
}
