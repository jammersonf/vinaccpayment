<?php

namespace App\Units;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel;

class ConsoleKernel extends Kernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call('App\Units\Core\Http\Controllers\ScheduleController@remaining')
                 ->dailyAt('15:02');
                 
        $schedule->call('App\Units\Core\Http\Controllers\ScheduleController@late')
                 ->dailyAt('15:02');

        $schedule->call('App\Units\Core\Http\Controllers\ScheduleController@birthday')
                 ->dailyAt('15:05');

        $schedule->call('App\Units\Core\Http\Controllers\ScheduleController@unpaid')
                 ->dailyAt('15:10');

        $schedule->call('App\Units\Core\Http\Controllers\ScheduleController@renewpap')
                 ->monthlyOn(1, '14:02');
    }

    /**
     * Register the Closure based commands for the application.
     */
    protected function commands()
    {
    }
}
