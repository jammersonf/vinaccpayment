<?php

namespace App\Units\Sends;

use Codecasts\Support\Http\Routing\RouteFile;

class Routes extends RouteFile
{
    /**
     * Declare Web Routes.
     */
    public function routes()
    {
        $this->accessRoutes();
    }

    protected function accessRoutes()
    {
        $this->router->group(['prefix' => 'envios'], function() {
            $this->router->get('{type}', 'SendController@index')->name('send.index');
            $this->router->post('{type}/search', 'SendController@search')->name('send.search');
        });
    }
}
