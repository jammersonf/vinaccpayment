@extends('core::layouts.app')

@section('title', 'Envios')

@section('page-actions')
@endsection

@section('content')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Filtros</h3>
            </div>
            <div class="panel-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    {!! Form::open(['route' => ['send.search', $type]]) !!}
                        <div class="input-group">
                            <input type="date" name="dt" class="form-control date" style="width: 170px" value="{{$dt}}" />
                            <input type="date" name="dt2" class="form-control date" style="width: 170px" value="{{$dt2 or ''}}" />
                            <button class="btn btn-primary search" type="submit">Pesquisar</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Listagem</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Mantenedor</th>
                            <th>Tipo</th>
                            <th>Texto</th>
                            <th>Data</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($data as $send)
                        @php
                        $name = $type == 'carne' ? 
                                    $send->maintainer()->name : 
                                    $send->maintainer->name;
                        @endphp
                            <tr>
                                <td>{{ $send->id }}</td>
                                <td>{{ $name }}</td>
                                <td>{{ $send->type }}</td>
                                <td>{{ $send->text }}</td>
                                <td>{{ date('d/m/Y H:i:s', strtotime($send->date)) }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">Vazio</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
;(function($)
{
    'use strict';

    $(document).ready(function() {
        $(".delete").click(function () {
            var r = confirm("Deseja mesmo excluir?");
            if (! r) {
                return false;
            }
        });
    });
})(window.jQuery);
</script>
@endsection
