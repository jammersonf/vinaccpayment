<?php

namespace App\Units\Sends\Providers;

use Codecasts\Support\Units\ServiceProvider;

class UnitServiceProvider extends ServiceProvider
{
    
    protected $alias = 'send';

    protected $hasViews = true;

    protected $hasTranslations = true;

    protected $providers = [
        EventServiceProvider::class,
        RouteServiceProvider::class,
    ];
}
