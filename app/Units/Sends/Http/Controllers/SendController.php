<?php

namespace App\Units\Sends\Http\Controllers;

use App\Domains\OfferPaymentEnvios\OfferPaymentEnvio;
use App\Domains\Sendss\Sends;
use App\Domains\Sendss\SendsRepository;
use App\Domains\SlipLogs\SlipLog;
use Codecasts\Support\Http\Controller;
use Illuminate\Http\Request;

class SendController extends Controller
{

    protected $carneModel;
    protected $papModel;

    public function __construct(SlipLog $sl, OfferPaymentEnvio $ope)
    {
        $this->carneModel = $sl;
        $this->papModel = $ope;
    }

    public function index($type)
    {
        $model = $type == 'carne' ? 'carneModel' : 'papModel';
        $dt = date('Y-m-d');
        $dt2 = '';
        $data = $this->$model->whereDate('date', date('Y-m-d'))->get();
        
        return view('send::index', compact('data', 'type', 'dt', 'dt2'));
    }

    public function search($type, Request $request)
    {
        $input = $request->all();
        $model = $type == 'carne' ? 'carneModel' : 'papModel';
        $dt = $input['dt'];
        $dt2 = $input['dt2'];
        $math = $input['dt2'] ? '>=' : '=';

        $data = $this->$model->whereDate('date', $math, $input['dt']);
        if ($input['dt2']) {
            $data = $data->whereDate('date', '<=', $input['dt2']);
        }
        $data = $data->get();

        return view('send::index', compact('data', 'type', 'dt', 'dt2'));
    }
}
