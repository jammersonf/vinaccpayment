<?php

namespace App\Units\API\Providers;

use Codecasts\Support\Units\ServiceProvider;

class UnitServiceProvider extends ServiceProvider
{
    protected $alias = 'api';

    protected $hasViews = false;

    protected $hasTranslations = false;

    protected $providers = [
        EventServiceProvider::class,
        RouteServiceProvider::class,
    ];
}
