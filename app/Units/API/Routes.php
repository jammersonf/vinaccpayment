<?php

namespace App\Units\API;

use Codecasts\Support\Http\Routing\RouteFile;

class Routes extends RouteFile
{
    /**
     * Declare Web Routes.
     */
    public function routes()
    {
        $this->accessRoutes();
    }

    protected function accessRoutes()
    {
        $this->router->group(['prefix' => 'api'], function () {
            $this->router->get('users', 'ApiController@users');
            $this->router->get('riders', 'ApiController@riders');
        });
    }
}
