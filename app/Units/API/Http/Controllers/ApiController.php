<?php

namespace App\Units\API\Http\Controllers;

use App\Domains\Users\User;
use Codecasts\Support\Http\Controller;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function __construct()
    {
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        }
    }

    public function users()
    {
        return User::orderId()->active()->user()->get();
    }


    public function riders()
    {
        return User::orderId()->active()->rider()->get();
    }
}
