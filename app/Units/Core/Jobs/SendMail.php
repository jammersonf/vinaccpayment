<?php

namespace App\Units\Core\Jobs;

use App\Domains\Settings\Setting;
use App\Domains\SlipLogs\SlipLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendMail implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $url;
    protected $slip;
    protected $user;
    protected $type;

    public function __construct($slip, $user, $type)
    {
        $this->url = 'https://www51.e-goi.com/api/public/mail/send';
        $this->slip = $slip;
        $this->user = $user;
        $this->type = $type;
    }    

    public function handle()
    {
        $user = $this->user;
        $msg = $this->getMsg($this->type);
        $params = $this->getArr($user, $msg);

        \Guzzle::post($this->url, [ \GuzzleHttp\RequestOptions::JSON => $params]);

        SlipLog::create(['type' => $this->type, 'text' => 'email enviado', 'slips_id' => $this->slip, 'date' => date('Y-m-d H:i:s')]);

        return true;
    }

    public function getMsg($type)
    {
        return Setting::where('type', $type)->first();
    }

    public function getArr($user, $msg)
    {
        return [
            //'domain' => 'vinacc.org.br',
            'subject' => 'VINACC',
            'email' => $user->email,
            'senderHash' => 'ceda28f4f2b99a4b81220a90af9586c3',
            'message' => [
                'html' => $msg->value
            ],
            'openTrackingEnabled' => true,
            'clickTrackingEnabled' => true,
            'priority' => 'non-urgent',
            'apikey' => config('api.egoi.key')
        ];
    }
}
