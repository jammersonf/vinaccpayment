<?php

namespace App\Units\Core\Jobs;

use App\Domains\Settings\Setting;
use App\Domains\SlipLogs\SlipLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendSMS implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $url;
    protected $slip;
    protected $user;
    protected $type;

    public function __construct($slip, $user, $type)
    {
        $this->url = 'https://www51.e-goi.com/api/public/sms/send';
        $this->slip = $slip;
        $this->user = $user;
        $this->type = $type;
    }

    public function handle()
    {
        $user = $this->user;
        $msg = $this->getMsg($this->type);

        $params = $this->getArr($user, $msg);

        \Guzzle::post($this->url, [\GuzzleHttp\RequestOptions::JSON => $params]);

        if ($this->slip) {
            SlipLog::create(['type' => $this->type, 'text' => 'mensagem enviada', 'slips_id' => $this->slip, 'date' => date('Y-m-d H:i:s')]);
        }
        
        return true;
    }

    public function getMsg($type)
    {
        return Setting::where('type', $type)->first();
    }

    public function getArr($user, $msg)
    {
        return [
            "mobile" => "55-".$user->phone,
            "senderHash" => "a8a976b2d9c0ad578ddce8b52ae9c887",
            "subject" => "VINACC",
            "message" => $msg->value,
            "options" => [
                "gsm0338" => true,
                "maxCount" => 0
            ],
            "priority" => "non-urgent",
            "apikey" => config('api.egoi.key')
        ];
    }
}
