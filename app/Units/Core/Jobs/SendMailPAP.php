<?php

namespace App\Units\Core\Jobs;

use App\Domains\OfferPaymentEnvios\OfferPaymentEnvio;
use App\Domains\Settings\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendMailPAP implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $url;
    protected $maintainer;
    protected $offerp;
    protected $type;
    protected $infoAdicional;

    public function __construct($maintainer, $type, $offerp = 0, $info = 0)
    {
        $this->url = 'https://www51.e-goi.com/api/public/mail/send';
        $this->maintainer = $maintainer;
        $this->type = $type;
        $this->offerp = $offerp;
        $this->infoAdicional = $info;
    }

    public function handle()
    {
        $maintainer = $this->maintainer;
        $msg = $this->getMsg($this->type);
        $params = $this->getArr($maintainer, $msg);
        if ($maintainer->email) {
            \Guzzle::post($this->url, [ \GuzzleHttp\RequestOptions::JSON => $params]);

            OfferPaymentEnvio::create([
                'maintainer_id' => $maintainer->id,
                'payment_id' => is_object($this->offerp) ? $this->offerp->id : 0,
                'type' => $this->type,
                'text' => 'email enviado'
            ]);
        }
        return true;
    }

    public function getMsg($type)
    {
        $text = Setting::where('type', $type)->first()->value;
        if ($type == 'recibopap') {
            $offerp = $this->offerp;
            $offer = $this->offerp->offer;
            
            $text = $text." <br>  Agente: ".$offer->rider->name."<br>";
            if($offerp->value) {
                $text = $text." Valor pago: ".$offerp->value."<br>";
            }
        } else if($type == 'reagendamento') {
            $text = $text." <br> Reagendamento: ".date('d/m/Y', strtotime($this->infoAdicional));
        }

        return $text;
    }

    public function getArr($maintainer, $msg)
    {
        return [
            //'domain' => 'vinacc.org.br',
            'subject' => 'VINACC',
            'email' => $maintainer->email,
            'senderHash' => 'ceda28f4f2b99a4b81220a90af9586c3',
            'message' => [
                'html' => $msg
            ],
            'openTrackingEnabled' => true,
            'clickTrackingEnabled' => true,
            'priority' => 'non-urgent',
            'apikey' => config('api.egoi.key')
        ];
    }
}
