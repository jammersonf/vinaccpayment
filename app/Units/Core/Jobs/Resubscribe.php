<?php

namespace App\Units\Core\Jobs;

use App\Domains\Settings\Setting;
use App\Domains\SlipLogs\SlipLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class Resubscribe implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $url;
    protected $email;

    public function __construct($email)
    {
        $this->url = 'https://www51.e-goi.com/api/public/mail/resubscribe';
        $this->email = $email;
    }    

    public function handle()
    {
        $params = $this->getArr();

        $response = \Guzzle::post($this->url, [ \GuzzleHttp\RequestOptions::JSON => $params]);

        SlipLog::create(['type' => 'resubscribe', 'text' => $this->email.' resubscribed', 'slips_id' => 0, 'date' => date('Y-m-d H:i:s')]);

        dd((string) $response->getBody());

        return true;
    }

    public function getArr()
    {
        return [
            'apikey' => config('api.egoi.key'),
            'contact' => $this->email
        ];
    }
}
