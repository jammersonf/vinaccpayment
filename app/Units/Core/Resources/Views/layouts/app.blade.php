<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link rel="icon" href="{{ url('favicon.ico') }}">

    <title>{{ config('app.name') }}</title>
    <link rel="stylesheet" href="{{ url('nifty/nifty.min.css') }}"/>
    <link rel="stylesheet" href="{{ elixir('css/back.css') }}"/>
    <link rel="stylesheet" href="{{ url('css/geral.css') }}"/>
    <style type="text/css" media="print">
    .dontprint
    { display: none; }
    </style>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    @if (\Auth::check())
        @include('core::layouts.partials.navbar')
        @include('core::layouts.partials.header')
    @endif
    <main>
        <div class="container">
            @include('flash::message')
            @if (\Auth::check() && (\Auth::user()->profile == 'Admin'))
                <!--<div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="alert alert-success">
                                Temos um boleto com vencimento para o dia 16/03 <a href="https://visualizacao.gerencianet.com.br/emissao/73874_1261_TAFO2/A4XB-73874-155105579-MARRA5" target="_BLANK" style="color: white"><b>Clique aqui para Imprimir</b></a>
                            </div>
                        </div>
                    </div>
                </div> -->
            @endif
            @yield('content')
        </div>
    </main>
    <script src="{{ elixir('js/back/vendor.js') }}"></script>
    @yield('scripts')
</body>
</html>
