<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">{{ config('app.name') }}</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            @if(auth()->user()->isAdmin())
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Mantenedores<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('user.index') }}">Carnê</a></li>
                            <li><a href="{{ route('maintainer.index') }}">Porta a porta</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Ofertas<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('ofertas/pagas') }}">Carnê</a></li>
                            <li><a href="{{ route('offer.payment.payments') }}">Porta a porta</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Envios<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('send.index', ['carne']) }}">Carnê</a></li>
                            <li><a href="{{ route('send.index', ['pap']) }}">Porta a porta</a></li>
                        </ul>
                    </li>
                </li>
                </ul>
            @endif
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        @if(auth()->user()->isAdmin())
                            <li><a href="{{route('core.texts')}}">Textos Mensagens</a></li>
                            <li><a href="{{route('user.admin')}}">Administradores</a></li>
                            <li><a href="{{route('user.rider')}}">Agentes</a></li>
                            <li><a href="{{route('church.index')}}">Igrejas</a></li>
                            <li><a href="{{route('ref.index')}}">Origens</a></li>
                        @endif
                        <li><a href="/logout">Sair</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

