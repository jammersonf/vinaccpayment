@extends('core::layouts.app')

@section('title', 'Textos')

@section('page-actions')
    <a href="{{ route('index') }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Voltar</a>
@stop

@section('content')
<div class="container">
    <div class="row">
        <h5 align="center">Os textos de SMS devem conter no máximo 140 caracteres</h5>
        <br>
        {{Form::open(['route' => 'core.text.update'])}}
        @foreach ($texts as $text)
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="box-body">
                <div class="form-group input_fields_wrap">
                    <label>{{$text->type}}</label>
                    <textarea name="{{$text->id}}" rows="5" maxlength="140" class="form-control" required>{!! $text->value !!}</textarea>
                </div>
            </div>
        </div>
        @endforeach
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Atualizar</button>
        </div>
        {{ Form::close()}}
    </div>
    <br><br>
</div>
@endsection

@section('scripts')
    <script src="//cdn.ckeditor.com/4.7.2/standard/ckeditor.js"></script>
@endsection