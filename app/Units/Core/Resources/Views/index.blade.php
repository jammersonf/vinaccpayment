<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link rel="icon" href="{{ url('favicon.ico') }}">

    <title>{{ config('app.name') }}</title>
    <link rel="stylesheet" href="{{ url('nifty/nifty.min.css') }}"/>
    <link rel="stylesheet" href="{{ elixir('css/back.css') }}"/>
    <style type="text/css" media="print">
        .dontprint
        { display: none; }
    </style>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
@include('core::layouts.partials.header')

<main>
    @include('core::layouts.partials.messages')
    @include('core::layouts.partials.errors')
    @yield('content')
</main>
<script src="{{ elixir('js/back/vendor.js') }}"></script>
@yield('scripts')
</body>
</html>
