<?php

namespace App\Units\Core;

use App\Units\Core\Http\Controllers\CoreController;
use Codecasts\Support\Http\Routing\RouteFile;

/**
 * Web Routes.
 *
 * This file is where you may define all of the routes that are handled
 * by your application. Just tell Laravel the URIs it should respond
 * to using a Closure or controller method. Build something great!
 */
class Routes extends RouteFile
{
    /**
     * Declare Web Routes.
     */
    public function routes()
    {
        $this->accessRoutes();
    }

    protected function accessRoutes()
    {
        $this->router->get('app', 'CoreController@app');
        $this->router->get('redis', 'CoreController@redis');
        $this->router->get('schedule/remaining', 'ScheduleController@remaining');
        $this->router->get('schedule/late', 'ScheduleController@late');
        $this->router->get('schedule/renewpap', 'ScheduleController@renewpap');
        $this->router->get('resubscribe/{email}', 'CoreController@resubscribe')->name('resubscribe');
        $this->router->get('textos', 'CoreController@texts')->name('core.texts');
        $this->router->get('boleto', 'CoreController@slip')->name('core.slip');
        $this->router->post('texto/update', 'CoreController@update')->name('core.text.update');
    }
}
