<?php

namespace App\Units\Core\Http\Controllers;

use App\Domains\Settings\Setting;
use App\Units\Core\Jobs\Resubscribe;
use Illuminate\Http\Request;

class CoreController
{
    private $user;

    public function __construct()
    {
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        }

        $this->user = \Auth::user();
    }

    public function app()
    {
        return response()->download(public_path('app.apk'));
    }

    public function resubscribe($email)
    {
        dispatch(new Resubscribe($email));
    }

    public function redis()
    {
        return \Redis::lrange('queues:$queueName', 0, -1);
    }

    public function slip()
    {
        $dom = new \PHPHtmlParser\Dom();
        $dom->loadFromUrl('https://api.pagar.me/1/boletos/live_cjo4ey1y31xkzkm3dwxaeyamx');
        $table = $dom->find('table');
        $arr['header']  = ['html' => $table[2], 'keys' => ['codigoBanco' => 2, 'linhaDigitavel' => 4]];
        $arr['body0']   = ['html' => $table[10], 'keys' => ['local' => 2, 'vencimento' => 3]];
        $arr['body1']   = ['html' => $table[11], 'keys' => ['beneficiario' => 2, 'codigo' => 3]];
        $arr['body2']   = ['html' => $table[12], 'keys' => ['data' => 6, 'documento' => 7, 'especie' => 8, 'aceite' => 9, 'processamento' => 10, 'numero' => 11]];
        $arr['body3']   = ['html' => $table[13], 'keys' => ['carteira' => 7, 'valor' => 11]];
        $arr['body4']   = ['html' => $table[14], 'keys' => ['instrucoes' => 0]];
        $arr['pagador'] = ['html' => $table[6],  'keys' => ['pagador' => 1]];
        $data = [];
        foreach ($arr as $key => $a) {
            $keys = $a['keys'];
            foreach ($keys as $key => $k) {
                $data[$key] = $a['html']->find('td')[$k]->innerHtml;
            }
        }
        $data['instrucoes'] = str_replace(['<div class="ctN pL10">Instruções (Texto de responsabilidade do beneficiário)</div> <div class="cpN pL10">', '</div>'], '', $data['instrucoes']);
        $data['pagador'] = explode('<br />', $data['pagador']);
        $data['codigoBarras'] = $dom->find('.barcode')[0]->outerHtml;

        $render = view('slip::boletoraw', compact('data'))->render();
        return $render.$render.$render.$render.$render.$render.$render.$render.$render;
    }

    public function texts()
    {
        $texts = Setting::all();

        return view('core::texts', compact('texts'));
    }

    public function update(Request $request)
    {
        $input = $request->except(['_token']);
        foreach ($input as $key => $t) {
            Setting::whereId($key)->update(['value' => $t]);
        }

        flash('Feito. Textos autalizados')->success();
        return redirect()->route('user.index');
    }
}
