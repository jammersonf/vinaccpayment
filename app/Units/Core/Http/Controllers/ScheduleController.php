<?php

namespace App\Units\Core\Http\Controllers;

use App\Domains\Logs\Log;
use App\Domains\Maintainers\Maintainer;
use App\Domains\OfferPayments\OfferPayment;
use App\Domains\Offers\Offer;
use App\Domains\Slips\Slip;
use App\Units\Core\Jobs\SendMail;
use App\Units\Core\Jobs\SendMailPAP;
use App\Units\Core\Jobs\SendSMS;
use App\Units\Core\Jobs\SendSMSPAP;
use Codecasts\Support\Http\Controller;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    public function remaining()
    {
        $slips = Slip::where('date', date('Y-m-d', strtotime('+3 days')))->where('status', 'waiting_payment')->get();
        foreach ($slips as $s) {
            if (config('app.send')) {
                if ($s->user->send_mail) {
                    dispatch(new SendMail($s->id, $s->user, 'lembrarpagamento'));
                }
                if ($s->user->send_phone) {
                    dispatch(new SendSMS($s->id, $s->user, 'lembrarpagamento'));
                }
            }
        }
    }

    public function late()
    {
        $slips = Slip::where('date', date('Y-m-d', strtotime('-3 days')))->where('status', 'waiting_payment')->get();
        foreach ($slips as $s) {
            if (config('app.send')) {
                if ($s->user->send_mail) {
                    dispatch(new SendMail($s->id, $s->user, 'boletoatrasado'));
                }
                if ($s->user->send_phone) {
                    dispatch(new SendSMS($s->id, $s->user, 'boletoatrasado'));
                }
            }
        }
    }

    public function birthday()
    {
        $maintainers = Maintainer::where('nascimento', 'like', '%'.date('m-d'))->get();
        foreach ($maintainers as $maintainer) {
            if (config('app.send')) {
                dispatch(new SendMailPAP($maintainer, 'aniversariante'));
                dispatch(new SendSMSPAP($maintainer, 'aniversariante'));
            }
        }
    }

    public function unpaid()
    {
        $offers = OfferPayment::where('date', date('Y-m-d', strtotime('-26 days')))
                              ->where('status', 'pendente')
                              ->get();

        foreach ($offers as $offerp) {
            if (config('app.send')) {
                dispatch(new SendMailPAP($offerp->maintainer, 'naopago'));
                dispatch(new SendSMSPAP($offerp->maintainer, 'naopago'));
            }
        }
    }

    public function renewpap()
    {
        $offersP = OfferPayment::whereNumber('12')
                              ->whereYear('date', date('Y'))
                              ->whereMonth('date', date('m'))
                              ->get();
        foreach($offersP as $offerp) {
            $maintainer = Maintainer::find($offerp->offer->maintainer_id);

            (new Offer())->createPayments($maintainer, true);
            
            Log::create([
                'type' => 'renewpap',
                'text' => 'oferta '.$offerp->offer->id.' renovada',
                'slips_id' => 0,
                'date' => date('Y-m-d H:i:s')
            ]);
        }
    }
}
