<?php

namespace App\Units\OfferPaymentLog\Http\Controllers;

use Codecasts\Support\Http\Controller;
use Illuminate\Http\Request;

class OfferPaymentLogController extends Controller
{
    public function index()
    {
        return view('OfferPaymentLog::index');
    }

    public function toUsd($value)
    {
        $number = str_replace('.', '', $value);
        return str_replace(',', '.', $number);
    }
}
