<?php

namespace App\Units\OfferPaymentLog;

use Codecasts\Support\Http\Routing\RouteFile;

class Routes extends RouteFile
{
    /**
     * Declare Web Routes.
     */
    public function routes()
    {
        $this->accessRoutes();
    }

    protected function accessRoutes()
    {
        $this->router->group(['prefix' => 'ofertas/detalhes/logs'], function () {
            $this->router->get('/', 'OfferPaymentLogController@index')->name('offer.payment.log.index');
        });
    }
}
