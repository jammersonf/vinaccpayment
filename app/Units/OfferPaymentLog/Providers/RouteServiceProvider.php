<?php

namespace App\Units\OfferPaymentLog\Providers;

use App\Units\OfferPaymentLog\Routes;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Units\OfferPaymentLog\Http\Controllers';

    public function boot()
    {
        parent::boot();
    }

    public function map()
    {
        (new Routes([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ]))->register();
    }
}
