<?php

namespace App\Units\User;

use Codecasts\Support\Http\Routing\RouteFile;

class Routes extends RouteFile
{
    /**
     * Declare Web Routes.
     */
    public function routes()
    {
        $this->accessRoutes();
    }

    protected function accessRoutes()
    {
        $this->router->group(['prefix' => 'mantenedores', 'middleware' => 'auth'], function () {
            $this->router->get('/', 'UserController@index')->name('user.index');
            $this->router->get('admin', 'UserController@admin')->name('user.admin');
            $this->router->get('bloqueados', 'UserController@block')->name('user.block');
            $this->router->get('pendencias', 'UserController@pendencies')->name('user.pendencies');
            $this->router->post('pesquisar', 'UserController@search')->name('user.search');
            $this->router->get('{id}/editar', 'UserController@show')->name('user.show');
            $this->router->post('{id}/editar', 'UserController@update')->name('user.update');
            $this->router->get('acao/{id}/{action}', 'UserController@action')->name('user.action');
            $this->router->post('tickets/print', 'TicketController@print')->name('user.ticket.print');
        });

        $this->router->group(['prefix' => 'motoqueiros', 'middleware' => 'auth'], function () {
            $this->router->get('/', 'RiderController@index')->name('user.rider');
            $this->router->get('{id}/panel/{list?}/{view?}/{month?}', 'RiderController@panel')->name('user.rider.panel');
            $this->router->get('{id}/print/{list}/{month?}', 'RiderController@print')->name('rider.print');
            $this->router->get('{id}/payment/{list}/reagendamentos', 'RiderController@reschedules')->name('rider.reschedules');
            $this->router->get('{id}/payment/{list}/{month?}', 'RiderController@payment')->name('rider.payment');
            $this->router->post('set/user', 'RiderController@setUser')->name('user.rider.set');
            $this->router->post('search', 'RiderController@search')->name('rider.search');
        });

        $this->router->get('mantenedor/cadastrar', 'UserController@create')->name('user.create');
        $this->router->post('mantenedor/cadastrar', 'UserController@store')->name('user.store');
        $this->router->get('mantenedor/{id?}/painel', 'UserController@panel')->name('user.panel');
    }
}
