<?php

namespace App\Units\User\Http\Controllers;

use App\Domains\Users\User;
use Codecasts\Support\Http\Controller;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    public function print(Request $request)
    {
        $input = $request->all();
        if ($input['date2']) {
            $users = User::whereBetween('date', [$input['date1'], $input['date2']])->get();
        } else {
            $users = User::where('date', $input['date1'])->get();
        }

        return view('user::tickets', compact('users'));
    }
}
