<?php

namespace App\Units\User\Http\Controllers;

use App\Domains\Churches\Church;
use App\Domains\Payments\Payment;
use App\Domains\Refs\Ref;
use App\Domains\Slips\Slip;
use App\Domains\Users\User;
use App\Domains\Users\UserRepository;
use App\Units\Payment\Events\PaymentStored;
use App\Units\Slip\Events\SlipChanged;
use App\Units\Slip\Services\SlipService;
use Codecasts\Support\Http\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;


class UserController extends Controller
{
    public function __construct()
    {
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        }
    }

    public function index()
    {
        $users = User::orderId()->active()->user()->paginate();
        $count = User::active()->user()->count();

        $tp = 'Mantenedores';
        
        return view('user::index', compact('users', 'tp', 'count'));
    }

    public function search(Request $request)
    {
        $users = User::where('email', 'like', $request->get('usu').'%')
                    ->orWhere('name', 'like', '%'.$request->get('usu').'%')
                    ->active()
                    ->paginate();
        $count = User::where('email', 'like', $request->get('usu').'%')
                    ->orWhere('name', 'like', '%'.$request->get('usu').'%')
                    ->active()
                    ->count();

        $tp = 'Usuários';
        
        return view('user::index', compact('users', 'tp', 'count'));
    }

    public function admin()
    {
        $users = User::orderId()->active()->admin()->paginate();
        $count = User::active()->admin()->count();

        $tp = 'Administradores';
        
        return view('user::index', compact('users', 'tp', 'count'));
    }

    public function block()
    {
        $users = User::orderId()->inactive()->paginate();
        $count = User::inactive()->count();

        $tp = 'Bloqueados';
        
        return view('user::index', compact('users', 'tp', 'count'));
    }

    public function panel($id)
    {
        $user = User::find($id);
        
        $payments = Payment::with('user')->where('user_id', $id)->get();
        $singles = Slip::singles()->byUser($id)->get();
        $offers = Slip::offers()->byUser($id)->get();
        $keys = $payments->map(function ($p) {
            return $keys[] = $p->id;
        })->toArray();
        $slip = Slip::whereIn('payment_id', $keys)->whereYear('date', '=', date('Y'))->whereMonth('date', '=', date('m'))->first();

        return view('user::dashboard', compact('user', 'payments', 'singles', 'slip', 'offers'));
    }

    public function create()
    {
        $churches = Church::orderName()->get()->pluck('name', 'id');
        $refs = Ref::orderName()->get()->pluck('name', 'id');

        return view('user::create', compact('churches', 'refs'));
    }

    public function store(Request $request)
    {
        try {
            $input = $request->except('_token');
            $input['send_mail'] = $request->has('send_mail') ? 1 : 0;
            $input['send_phone'] = $request->has('send_phone') ? 1 : 0;
            $input['password'] = bcrypt(str_replace(['.', '-'], '', $input['cpf']));
            $input['phone'] = str_replace(['(', ')', ' ', '-'], '', $input['phone']);
            $get = User::where('cpf', $input['cpf'])->orWhere('email', $input['email'])->first();
            if ($get) {
                flash('Usuário já existente: '.$get->name, 'warning');
                return auth()->check() ? redirect()->route('user.index') : redirect()->route('user.create') ;
            } else {
                $user = User::create($input);
            }
            flash('Cadastrado com Sucesso', 'success');
            if (! auth()->check()) {
                flash("Bem vindo(a) ".$user->name, 'success');
                auth()->loginUsingId($user->id);
            }
        } catch (\Exception $e) {
            flash('Problema ao cadastrar Userro '.$e, 'danger');
            return redirect()->route('user.create');
        }
        return redirect()->route('payment.create', $user->id);
    }

    public function show($id)
    {
        $user = User::find($id);
        $churches = Church::orderName()->get()->pluck('name', 'id');
        $refs = Ref::orderName()->get()->pluck('name', 'id');

        return view('user::edit', compact('user', 'churches', 'refs'));
    }

    public function pendencies()
    {
        $slips = Slip::with('user')->where('date', '<', date('Y-m-d'))->unpaid()->get();
        $count = $slips->count();
        $arr = [];

        foreach ($slips as $slip) {
            $arr[$slip->user_id][] = $slip;
        }

        return view('user::pendencies', compact('arr', 'count'));
    }

    public function update($id, Request $request)
    {
        try {
            $input = $request->except('_token');
            $input['send_mail'] = $request->has('send_mail') ? 1 : 0;
            $input['send_phone'] = $request->has('send_phone') ? 1 : 0;
            $get = User::where('cpf', $input['cpf'])->orWhere('email', $input['email'])->first();
            if ($get && $get->id != $id) {
                flash('Usuário já existente', 'warning');
                return auth()->check() ? redirect()->route('user.index') : redirect()->route('user.create') ;
            } else {
                $input['phone'] = str_replace(['(', ')', ' ', '-'], '', $input['phone']);
                User::find($id)->update($input);
            }
            
            flash('Atualizado com Sucesso', 'success');
        } catch (\Exception $e) {
            flash('Problema ao atualizar Userro '.$e, 'danger');
        }

        return redirect()->route('user.index');
    }

    public function action($id, $action)
    {
        try {
            User::find($id)->update(['status' => $action]);

            flash("Usuário Alterado!", 'success');
        } catch (\Exception $e) {
            flash("problema ao apagar ".$e, 'danger');
        }

        return redirect()->route('user.index');
    }
}
