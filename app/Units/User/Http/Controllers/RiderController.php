<?php

namespace App\Units\User\Http\Controllers;

use App\Domains\Churches\Church;
use App\Domains\OfferPaymentReschedule\OfferPaymentReschedule;
use App\Domains\Payments\Payment;
use App\Domains\Refs\Ref;
use App\Domains\Slips\Slip;
use App\Domains\Users\User;
use App\Domains\Users\UserRepository;
use App\Units\Payment\Events\PaymentStored;
use App\Units\Slip\Events\SlipChanged;
use Carbon\Carbon;
use Codecasts\Support\Http\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class RiderController extends Controller
{
    protected $user;
    protected $offerre;

    public function __construct(User $user, OfferPaymentReschedule $opr)
    {
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        }
        $this->user = $user;
        $this->offerre = $opr;
    }

    public function index()
    {
        $users = $this->user->orderId()->active()->rider()->paginate();
        $count = $this->user->active()->rider()->count();

        $tp = 'Motoqueiros';
        
        return view('user::index', compact('users', 'tp', 'count'));
    }

    public function panel($id, $list = '', $view = 'payment', $month = '')
    {
        $route = 'rider.print';
        if ($view == 'payment') {
            $route = 'rider.payment';
        }
        $list = $list ?: $this->getList();

        return redirect()->route($route, [$id, $list, $month]);
    }

    public function getList()
    {
        $d = date('d');
        $list = 5;
        if ($d >= $this->getDay(5) && $d < $this->getDay(10)) {
            $list = 5;
        } else if ($d >= $this->getDay(10) && $d < $this->getDay(15)) {
            $list = 10;
        } else if ($d >= $this->getDay(15) && $d < $this->getDay(20)) {
            $list = 15;
        } else if ($d >= $this->getDay(20) && $d < $this->getDay(25)) {
            $list = 20;
        } else if ($d >= $this->getDay(25) && $d < $this->getDay(30)) {
            $list = 25;
        } else {
            $list = 30;
        }
        /*if (auth()->user()->id == 218) {
            $list = 20;
        }*/

        return $list;
    }

    public function getDay($day)
    {
        return $this->checkSunday($day) ? $day + 2 : $day;
    }

    public function checkSunday($list)
    {
        $date = date("Y-m-$list");
        $d = date('w', strtotime($date));

        return $d == 0 ? true : false;
    }

    public function getListBefore($list)
    {
        $now = $list;
        switch ($list) {
            case 5:
                $now = 30;
                break;
            case 10:
                $now = 5;
                break;
            case 15:
                $now = 10;
                break;
            case 20:
                $now = 15;
                break;
            case 25:
                $now = 20;
                break;
            case 30:
                $now = 25;
                break;
        }

        return $now;
    }

    public function search(Request $request)
    {
        $input = $request->all();

        return redirect()->route('user.rider.panel', [$input['rider_id'], $input['list'], $input['type'], $input['month']]);
    }

    public function print($id, $list, $month = '')
    {
        $rider = $this->user->find($id);
        $listOption = [5 => 5, 10 => 10, 15 => 15, 20 => 20, 25 => 25, 30 => 30];
        $riders = $this->user->active()->rider()->pluck('name', 'id')->toArray();
        $monthOption = [
            '01' => 'Jan', '02' => 'Fev', '03' => 'Mar', '04' => 'Abr',
            '05' => 'Mai', '06' => 'Jun', '07' => 'Jul', '08' => 'Ago',
            '09' => 'Set', '10' => 'Out', '11' => 'Nov', '12' => 'Dez'
        ];
        $month = date('m');
        $maintainers = $rider->getMaintainersByList($list, true);
        
        return view('user::rider.index', compact('maintainers', 'riders', 'rider', 'list', 'listOption', 'monthOption', 'month'));
    }

    public function payment($id, $list, $month = '')
    {
        $rider = $this->user->find($id);
        $listOption = [5 => 5, 10 => 10, 15 => 15, 20 => 20, 25 => 25, 30 => 30];
        $monthOption = [
            '01' => 'Jan', '02' => 'Fev', '03' => 'Mar', '04' => 'Abr',
            '05' => 'Mai', '06' => 'Jun', '07' => 'Jul', '08' => 'Ago',
            '09' => 'Set', '10' => 'Out', '11' => 'Nov', '12' => 'Dez'
        ];
        if (auth()->user()->profile == 'motoqueiro' || ! $month) {
            $daysList = $this->checkSunday($this->getListBefore($list)) ? 28 : 27;
            if (date('d') <= 25) {
                $minus = Carbon::createFromFormat('Y-m-d', date("Y-m-d"))
                                ->subDay($daysList)
                                ->format('d');
                $date = Carbon::createFromFormat('Y-m-d', date("Y-m-$list"))
                              ->format('d');
                $month = $minus >= $date ? date('m') : date('m', strtotime('-1 month'));
            } else {
                $dateList = Carbon::createFromFormat('Y-m-d', date("Y-m-$list"))
                                ->addDay($daysList);
                $now = Carbon::now();
                
                $month = $dateList->greaterThanOrEqualTo($now) ? date('m') : date('m', strtotime('-1 month'));
            }
            //$month = $list <= 15 ? '02' : '01';
        }
        $users = $rider->getMaintainersByList($list, true);
        $count = $users->count();
        return view('user::rider.payment', compact('users', 'rider', 'id', 'list', 'listOption', 'count', 'month', 'monthOption', 'monthExtenso'));
    }

    public function reschedules($id, $list)
    {
        $data = $this->offerre
                     ->with(['offerp', 'offerp.offer', 'offerp.offer.maintainer'])
                     ->unpaid()
                     ->where('rider_id', $id)
                     //->whereList($list)
                     ->get();

        $today = $this->offerre
                     ->with(['offerp', 'offerp.offer', 'offerp.offer.maintainer'])
                     ->unpaid()
                     ->where('rider_id', $id)
                     ->where('date', date('Y-m-d'))
                     ->get();

        return view('user::rider.reschedules', compact('data', 'today'));
    }

    public function setUser(Request $request)
    {
        $input = $request->all();
        $user = $this->user->find($input['user_id']);
        $user->motoqueiro_id = $input['motoqueiro_id'];
        $user->save();

        return redirect()->route('user.panel', $input['user_id']);
    }
}
