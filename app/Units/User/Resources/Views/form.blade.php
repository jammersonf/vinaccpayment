<div class="row">
    <div class="col-xs-6 col-md-4">
        {!! Form::label('name', 'Nome')!!}
        {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
    </div>
    <div class="col-xs-4 col-md-2">
        {!! Form::label('email', 'Email') !!}
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-xs-4 col-md-1">
        {!! Form::label('documento', 'Tipo do documento') !!}<br>
        {!! Form::radio('documento', 'cpf', true, ['class' => 'doc']) !!} CPF |
        {!! Form::radio('documento', 'cnpj', false, ['class' => 'doc']) !!} CNPJ
    </div>
    <div class="col-xs-4 col-md-2">
        {!! Form::label('cpf', 'Documento') !!}
        {!! Form::text('cpf', null, ['class' => 'form-control cpf']) !!}
    </div>
    <div class="col-xs-4 col-md-2">
        {!! Form::label('phone', 'Telefone') !!}
        {!! Form::text('phone', null, ['class' => 'form-control phone']) !!}
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
        {!! Form::label('cep', 'CEP*')!!}
        {!! Form::text('cep', null, ['class' => 'form-control cep', 'placeholder' => 'CEP']) !!}
    </div>
    <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
        {!! Form::label('street', 'Endereço*')!!}
        {!! Form::text('street', null, ['class' => 'form-control endereco', 'placeholder' => 'Endereço']) !!}
    </div>
    <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
        {!! Form::label('neighborhood', 'Bairro*')!!}
        {!! Form::text('neighborhood', null, ['class' => 'form-control bairro', 'placeholder' => 'Bairro']) !!}
    </div>
    <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
        {!! Form::label('city', 'Cidade*')!!}
        {!! Form::text('city', null, ['class' => 'form-control cidade', 'placeholder' => 'Cidade']) !!}
    </div>
    <div class="col-xs-4 col-sm-4 col-md-1 col-lg-1">
        {!! Form::label('send_mail', 'Email')!!}
        {!! Form::checkbox('send_mail', 1) !!}
    </div>
    <div class="col-xs-4 col-sm-4 col-md-1 col-lg-1">
        {!! Form::label('send_phone', 'Phone')!!}
        {!! Form::checkbox('send_phone', 1) !!}
    </div>
    <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
        {!! Form::label('uf', 'Estado*')!!}
        {!! Form::text('uf', null, ['class' => 'form-control estado', 'placeholder' => 'Estado']) !!}
    </div>
    <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
        {!! Form::label('number', 'Número')!!}
        {!! Form::text('number', null, ['class' => 'form-control numero', 'placeholder' => 'Num']) !!}
    </div>
    <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
        {!! Form::label('complemento', 'Complemento')!!}
        {!! Form::text('complemento', null, ['class' => 'form-control numero']) !!}
    </div>

    <div class="col-xs-4 col-md-2">
        {!! Form::label('church_id', 'Igreja') !!}
        {!! Form::select('church_id', $churches, null, ['class' => 'form-control']) !!}
    </div>

    <div class="col-xs-4 col-md-2">
        {!! Form::label('ref_id', 'Origem') !!}
        {!! Form::select('ref_id', $refs, null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-xs-4 col-md-3">
        {!! Form::label('date', 'Início contrato') !!}
        @php
            $date = isset($user) ? $user->date : date('Y-m-d');
        @endphp
        {!! Form::date('date', $date, ['class' => 'form-control']) !!}
    </div>
    <div class="col-xs-4 col-md-3">
        {!! Form::label('nascimento', 'Data de nascimento') !!}
        {!! Form::date('nascimento', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-xs-4 col-md-3">
        {!! Form::label('obs', 'Observação') !!}
        {!! Form::text('obs', null, ['class' => 'form-control']) !!}
    </div>

    @if(auth()->check() && auth()->user()->isAdmin())
        <div class="col-xs-4 col-md-3">
            {!! Form::label('profile', 'Perfil') !!}
            {!! Form::select('profile', ['mantenedor' => 'Mantenedor', 'Admin' => 'Admin', 'motoqueiro' => 'Motoqueiro'], null, ['class' => 'form-control']) !!}
        </div>
    @else
        {!! Form::hidden('profile', 'mantenedor') !!}
    @endif
</div>
