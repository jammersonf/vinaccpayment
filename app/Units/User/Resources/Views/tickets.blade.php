<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        .test {
            border-style: solid;
            border-width: 0.5px;
            font-size: 11px;
            padding: 6px;
        }
    </style>
</head>
<body>
        @foreach($users as $user)
            <div class="col-xs-4 test">
                <?php
                    $name = explode(' - ', $user->name);
                    $name = isset($user[1]) ? $user[1] : $user->name;
                ?>
                <div class="col-xs-9" style="padding: 0;"><b>{{ $name }}</b></div>
                <div class="col-xs-3">{{ $user->id }}</div>
                <div class="col-xs-12" style="padding: 0;">{{ $user->street.', '.$user->number}}</div>
                <div class="col-xs-12" style="padding: 0;"><b>{{ $user->neighborhood}}</b></div>
                <div class="col-xs-4" style="padding: 0;"><b>{{ $user->cep }}</b></div>
                <div class="col-xs-8">{{ $user->city }} - {{$user->uf}}</div>
            </div>
        @endforeach
</body>
</html>