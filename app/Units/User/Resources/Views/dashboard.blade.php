@extends('core::layouts.app')

@section('title', 'Painel')

@section('page-actions')
    <a href="{{ route('payment.create', $user->id)}}" class="btn btn-primary"><i class="fa fa-money"></i> Carnê</a>
    <a href="{{ route('single.create', $user->id)}}" class="btn btn-primary"><i class="fa fa-money"></i> Avulsa</a>
    <a href="{{ route('offer.vale.create', $user->id)}}" class="btn btn-primary"><i class="fa fa-money"></i> Vale Oferta</a>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-xs-4 col-md-4">Nome: <b>{{$user->name}}</b></div>
            <div class="col-xs-4 col-md-4">Telefone: <b>{{$user->phone}}</b></div>
            <div class="col-xs-4 col-md-4">Endereço: <b>{{$user->street}}, {{$user->number}}</b></div>
        </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Oferta do mês</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Parcela</th>
                        <th>Oferta</th>
                        <th>Vencimento</th>
                        <th>Status</th>
                        <th>Data pagamento</th>
                        <th width="15%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @if($slip)
                        <tr class="{{ $slip->present()->statusClass }}">
                            <td>{{ $slip->id }}</td>
                            <td>{{ $slip->slip }}</td>
                            <td>{{ $slip->present()->value }}</td>
                            <td>{{ $slip->present()->date }}</td>
                            <td>{{ $slip->present()->status }}</td>
                            <td>{{ $slip->present()->datePaid }}</td>
                            <td class="td-actions">
                                <a href="{{$slip->boleto_url}}" title="Imprimir boleto" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-print"></i></a>
                                <a href="{{ route('slip.download', $slip->id) }}" title="fazer download" class="btn btn-primary btn-xs"><i class="fa fa-download"></i></a>
                                @if(auth()->user()->isAdmin() && ! $slip->datePaid)
                                    <a href="{{ route('slip.check', $slip->id) }}" class="btn btn-success btn-xs"><i class="fa fa-check"></i></a>
                                @endif
                            </td>
                        </tr>
                    @else
                        <tr>
                            <td colspan="3" class="text-center">Sem oferta para o mês atual</td>
                        </tr>
                    @endif
                </tbody>
            </table>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Boletos avulsos</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Parcela</th>
                        <th>Oferta</th>
                        <th>Vencimento</th>
                        <th>Status</th>
                        <th>Data pagamento</th>
                        <th width="15%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($singles as $single)
                        <tr class="{{ $single->present()->statusClass }}">
                            <td>{{ $single->id }}</td>
                            <td>{{ $single->slip }}</td>
                            <td>{{ $single->present()->value }}</td>
                            <td>{{ $single->present()->date }}</td>
                            <td>{{ $single->present()->status }}</td>
                            <td>{{ $single->present()->datePaid }}</td>
                            <td class="td-actions">
                                <a href="{{$single->boleto_url}}" title="Imprimir boleto" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-print"></i></a>
                                @if(auth()->user()->isAdmin())
                                    <a class="btn btn-danger btn-xs" title="Apagar boleto" href="{{ route('slip.destroy', $single->id)}}">
                                        <i class="fa fa-remove"></i>
                                    </a>
                                @endif
                                @if(! $single->datePaid)
                                    <a href="{{ route('slip.check', $single->id) }}" class="btn btn-success btn-xs"><i class="fa fa-check"></i></a>
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3" class="text-center">Ainda não foi emitido nenhum boleto avulso</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Vale Ofertas</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Parcela</th>
                        <th>Oferta</th>
                        <th>Vencimento</th>
                        <th>Status</th>
                        <th>Data pagamento</th>
                        <th width="15%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($offers as $offer)
                        <tr class="{{ $offer->present()->statusClass }}">
                            <td>{{ $offer->id }}</td>
                            <td>{{ $offer->slip }}</td>
                            <td>{{ $offer->present()->value }}</td>
                            <td>{{ $offer->present()->date }}</td>
                            <td>{{ $offer->present()->status }}</td>
                            <td>{{ $offer->present()->datePaid }}</td>
                            <td class="td-actions">
                                <a href="{{$offer->boleto_url}}" title="Imprimir boleto" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-print"></i></a>
                                @if(auth()->user()->isAdmin())
                                    <a class="btn btn-danger btn-xs" title="Apagar boleto" href="{{ route('slip.destroy', $offer->id)}}">
                                        <i class="fa fa-remove"></i>
                                    </a>
                                @endif
                                @if(! $offer->datePaid)
                                    <a href="{{ route('slip.check', $offer->id) }}" class="btn btn-success btn-xs"><i class="fa fa-check"></i></a>
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3" class="text-center">Ainda não foi emitido nenhum vale oferta</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            </div>
        </div>

        @include('payment::user')
        
    </div>
</div>
@stop
@section('scripts')
<script src="{{url('jquery.mask.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">

;(function($)
{
    'use strict';
    $(document).ready(function() {
    });
})(window.jQuery);
</script>
@stop
