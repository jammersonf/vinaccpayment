@extends('core::layouts.app')

@section('title', 'Dashboard')

@section('page-actions')
    <a href="{{ route('maintainer.create') }}" class="btn btn-fill btn-primary">Cadastrar</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Filtros</h3>
                        </div>
                        <div class="panel-body">
                        {!! Form::open(['route' => 'rider.search']) !!}
                                <div class="col-xs-2 col-md-2">
                                    {!! Form::select('rider_id', $riders, $rider->id, ['class' => 'form-control']) !!}
                                </div>
                                <div class="col-xs-2 col-md-2">
                                    {!! Form::select('list', $listOption, $list, ['class' => 'form-control']) !!}
                                </div>
                                <div class="col-xs-2 col-md-2">
                                    {!! Form::select('month', $monthOption, $month, ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::hidden('type', 'list')!!}
                                <button class="btn btn-default btn-sm search" type="submit">Ir</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Telefone</th>
                            <th>Parcela</th>
                            <th>Lista</th>
                            <th>Valor</th>
                            <th width="15%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $count = 0;
                        @endphp
                        @forelse($maintainers as $maintainer)
                        @if($maintainer->offer())
                        @php
                            $count++;
                        @endphp
                            <tr>
                                <td>{{ $maintainer->id }}</td>
                                <td>{{ $maintainer->name }}</td>
                                <td>{{ $maintainer->phone }}</td>
                                <td>{{ $maintainer->offer()->number }}</td>
                                <td>{{ $maintainer->list }}</td>
                                <td>{{ number_format($maintainer->value, 2, ',', '.') }}</td>
                                <td>{{ $maintainer->neighborhood }}</td>
                                <td class="td-actions">
                                    <a class="btn btn-primary btn-xs" title="editar informações" href="{{ route('maintainer.show', $maintainer->id)}}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a class="btn btn-primary btn-xs" title="Ver painel do usuário" href="{{ route('maintainer.panel', $maintainer->id)}}">
                                        <i class="fa fa-newspaper-o"></i>
                                    </a> 
                                </td>
                            </tr>
                        @endif
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">Vazio</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                </div>
                <div class="panel-footer">
                    <h3 class="panel-title">{{$rider->name}} / {{$list}} /{{ $count}}</h3>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
;(function($)
{
    'use strict';

    $(document).ready(function() {
        $(".delete").click(function () {
            var r = confirm("Deseja mesmo executar essa ação?");
            if (! r) {
                return false;
            }
        });
    });
})(window.jQuery);
</script>
@endsection
