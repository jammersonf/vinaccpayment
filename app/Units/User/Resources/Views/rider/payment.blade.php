@extends('core::layouts.app')

@section('title', 'Lista')

@section('page-actions')
    <a href="{{ route('user.rider.panel', $rider->id) }}" class="btn btn-fill btn-primary">Voltar</a>
    <a href="{{ route('rider.reschedules', [$rider->id, $list]) }}" class="btn btn-fill btn-primary">Reagendamentos</a>
@endsection
@section('content')
    <style>
        .test {
            border-style: solid;
            border-width: 0.5px;
            font-size: 16px;
            padding: 1px;
        }
    </style>
    <div class="row">
        <div class="alert alert-warning">
            <p>Não esquecer de verificar os reagendamentos!</p>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Filtros</h3>
                        </div>
                        <div class="panel-body">
                        {!! Form::open(['route' => 'rider.search']) !!}
                            {!! Form::hidden('rider_id', $rider->id) !!}
                            {!! Form::hidden('type', 'payment') !!}
                            <div class="col-xs-2 col-md-2">
                                {!! Form::select('list', $listOption, $list, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-xs-2 col-md-2">
                                @if(auth()->user()->profile == 'motoqueiro')
                                    {!! Form::hidden('month', 'payment') !!}
                                @else
                                    {!! Form::select('month', $monthOption, $month, ['class' => 'form-control']) !!}
                                @endif
                            </div>
                                <button class="btn btn-default btn-sm search" type="submit">Ir</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Lista: {{ $list }} | Motoqueiro: {{$rider->name}} | Mês: {{ $monthOption[$month] }}</h3>
                </div>
                <div class="table-responsive">
                    @php
                        $count = 0;
                        $paid = 0;
                        $waiting = 0;
                        $year = $list > 30 ? '2019' : '2020';
                        
                    @endphp
                    @foreach ($users as $user)
                        @if($user->offer($month, $year))
                            @php
                                $count++;
                                $offerP = $user->offer($month, $year);
                                $color = function($color) {
                                    switch($color) {
                                        case 'pago':
                                            return 'success';
                                            break;
                                    }
                                };
                                if ($offerP->status == 'pago') {
                                    $paid++;
                                } else {
                                    $waiting++;
                                }
                            @endphp
                            <a href="{{route('offerp.show', $offerP->id)}}" style="color: inherit;">
                                <div class="col-xs-6 test bg-{{$color($offerP->status)}}" id="{{ $user->neighborhood.$user->id}}" style="height: 185px;">
                                    <?php
                                        $name = explode(' - ', $user->name);
                                        $name = isset($user[1]) ? $user[1] : $user->name;
                                    ?>

                                    <div class="col-xs-12" style="padding: 0;"><b>{{ $user->neighborhood}}</b></div>
                                    <div class="col-xs-12" style="padding: 0;"><b>{{ $name }}</b></div>
                                    <div class="col-xs-12" style="padding: 0;">{{ $user->street.', '.$user->number}}</div>
                                    <div class="col-xs-5">{{$user->phone}}</div>
                                    <div class="col-xs-2">P{{$offerP->number}}</div>
                                    <div class="col-xs-5" style="color: red;">R$<b>{{number_format($user->value, 2, ',', '.') }}</b></div>
                                    <div class="col-xs-12">&nbsp;</div>
                                    <div class="col-xs-12" style="font-size: 12px; color: red">{{$user->obs}}</div>
                                </div>
                            </a>
                        @endif
                    @endforeach
                </div>
                <div class="panel-footer"><b>Total {{ $count }} | Aberto {{ $waiting}} | Pago {{$paid}}</b></div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
;(function($)
{
    'use strict';

    $(document).ready(function() {
        $(".delete").click(function () {
            var r = confirm("Deseja mesmo executar essa ação?");
            if (! r) {
                return false;
            }
        });
    });
})(window.jQuery);
</script>
@endsection
