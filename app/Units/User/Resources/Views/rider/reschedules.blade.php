@extends('core::layouts.app')

@section('title', 'Reagendamentos')

@section('page-actions')
    <a href="{{ url()->previous() }}" class="btn btn-fill btn-primary">Voltar</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Hoje</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Mantenedor</th>
                            <th>Lista</th>
                            <th>Valor</th>
                            <th>Data</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($today as $d)
                            <tr>
                                <td>{{ $d->id }}</td>
                                <td>{{ $d->offerp->offer->maintainer->name }}</td>
                                <td>{{ $d->list }}</td>
                                <td>{{ number_format($d->offerp->offer->maintainer->value, 2, ',', '.') }}</td>
                                <td>{{ date('d/m/Y', strtotime($d->date)) }}</td>
                                <td>{{ $d->offerp->status }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">Vazio</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Lista</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Mantenedor</th>
                            <th>Lista</th>
                            <th>Valor</th>
                            <th>Data</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($data as $d)
                            <tr>
                                <td>{{ $d->id }}</td>
                                <td>{{ $d->offerp->offer->maintainer->name }}</td>
                                <td>{{ $d->list }}</td>
                                <td>{{ number_format($d->offerp->offer->maintainer->value, 2, ',', '.') }}</td>
                                <td>{{ date('d/m/Y', strtotime($d->date)) }}</td>
                                <td>{{ $d->offerp->status }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">Vazio</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
;(function($)
{
    'use strict';

    $(document).ready(function() {
        $(".delete").click(function () {
            var r = confirm("Deseja mesmo executar essa ação?");
            if (! r) {
                return false;
            }
        });
    });
})(window.jQuery);
</script>
@endsection