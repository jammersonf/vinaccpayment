@extends('core::layouts.app')

@section('title', $tp)

@section('page-actions')
    <a href="{{ route('user.create') }}" class="btn btn-fill btn-primary">Cadastrar</a>
    @if(auth()->user()->isAdmin())
        <a href="{{ route('user.block') }}" class="btn btn-fill btn-primary">Bloqueados</a>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($tp !== 'Motoqueiros')
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Etiquetas</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            {!! Form::open(['route' => 'user.ticket.print']) !!}
                                <div class="input-group">
                                    <input type="date" name="date1" class="form-control" style="width: 170px"/>
                                    <input type="date" name="date2" class="form-control" style="width: 170px"/>
                                    <button class="btn btn-primary search" type="submit">Imprimir</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Filtros</h3>
                    </div>
                    <div class="panel-body">
                    {!! Form::open(['route' => 'user.search']) !!}
                        <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <div class="input-group">
                            <input type="text" name="usu" class="form-control input-sm date" style="width: 200px" placeholder="Nome ou Código" value="{{$usu or ''}}" />
                            </div>
                        </div>
                            <button class="btn btn-default btn-sm search" type="submit">Pesquisar</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Total de {{ $tp }}: {{ $count }}</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Telefone</th>
                            <th>Email</th>
                            <th>Perfil</th>
                            <th>Cadastro</th>
                            <th width="15%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->profile }}</td>
                                <td>{{ $user->present()->date }}</td>
                                <td class="td-actions">
                                    <a class="btn btn-primary btn-xs" title="editar informações" href="{{ route('user.show', $user->id)}}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    @if($user->profile == 'mantenedor')
                                        <a class="btn btn-primary btn-xs" title="Ver painel do usuário" href="{{ route('user.panel', $user->id)}}">
                                            <i class="fa fa-newspaper-o"></i>
                                        </a>
                                    @else
                                        <a class="btn btn-primary btn-xs" title="Ver painel do usuário" href="{{ route('user.rider.panel', $user->id)}}">
                                            <i class="fa fa-newspaper-o"></i>
                                        </a>
                                        <a class="btn btn-warning btn-xs" title="Ver painel do usuário" href="{{ route('user.rider.panel', [$user->id, 0, 'list', date('m')])}}">
                                            <i class="fa fa-newspaper-o"></i>
                                        </a>
                                    @endif 
                                    @if($user->status)
                                    <a class="btn btn-danger btn-xs delete" title="Bloquear usuário" href="{{ route('user.action', [$user->id, 0]) }}">
                                        <i class="fa fa-lock"></i>
                                    </a>
                                    @else
                                    <a class="btn btn-success btn-xs delete" title="Desbloquear usuário" href="{{ route('user.action', [$user->id, 1]) }}">
                                        <i class="fa fa-unlock"></i>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">Vazio</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>

                {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
;(function($)
{
    'use strict';

    $(document).ready(function() {
        $(".delete").click(function () {
            var r = confirm("Deseja mesmo executar essa ação?");
            if (! r) {
                return false;
            }
        });
    });
})(window.jQuery);
</script>
@endsection
