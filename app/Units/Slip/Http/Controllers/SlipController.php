<?php

namespace App\Units\Slip\Http\Controllers;

require_once base_path('vendor/pagarme/pagarme-php/Pagarme.php');

use App\Domains\Logs\Log;
use App\Domains\Slips\Slip;
use App\Domains\Slips\SlipRepository;
use App\Domains\Users\User;
use App\Units\Core\Jobs\SendMail;
use App\Units\Core\Jobs\SendSMS;
use Codecasts\Support\Http\Controller;
use Illuminate\Http\Request;

class SlipController extends Controller
{
    protected $slipRepository;

    public function __construct(SlipRepository $sr)
    {
        $this->slipRepository = $sr;
    }

    public function index()
    {
        $slips = Slip::orderId()->paginate();
        $render = true;

        return view('slip::index', compact('slips', 'render'));
    }

    public function show($id)
    {
        $slips = Slip::with(['payment', 'payment.user'])->where('payment_id', $id)->get();

        return view('slip::user', compact('slips'));
    }

    public function download($id)
    {
        $slip = Slip::find($id);

        return response()->download(public_path($slip->download));
    }

    public function payments($date = '')
    {
        $date = $date ?: date('Y-m-d');
        $slips = Slip::with(['user'])->where('date', $date)->get();
        
        return view('slip::payments', compact('slips', 'date'));
    }

    public function list($type)
    {
        $dt = date('Y-m-01');
        $dt2 = date('Y-m-d');
        $vale = false;
        $render = false;
        if ($type == 'pagas') {
            $slips = $this->slipRepository->getByDatePaid($dt, $dt2, $vale);
        } else {
            $slips = $this->slipRepository->getByDate($dt, $dt2, $vale);
        }
        if ($render) {
            $chart = $this->slipRepository->mountDataTable($dt, $dt2);
        }

        return view('slip::list', compact('slips', 'dt', 'dt2', 'chart', 'render', 'type', 'vale'));
    }

    public function search(Request $request)
    {
        $input = $request->all();
        $dt = $request->get('date1');
        $dt2 = $request->get('date2');
        $type = $request->get('type');
        $vale = isset($input['vale']);
        $render = false;
        if ($type == 'pagas') {
            $slips = $this->slipRepository->getByDatePaid($dt, $dt2, $vale);
        } else {
            $slips = $this->slipRepository->getByDate($dt, $dt2, $vale);
        }
        if ($render) {
            $chart = $this->slipRepository->mountDataTable($dt, $dt2);
        }

        return view('slip::list', compact('slips', 'dt', 'dt2', 'chart', 'render', 'type', 'vale'));
    }

    public function createTicket($month)
    {
        $slips = $this->slipRepository->getByMonth($month);
        
        return view('slip::tickets', compact('slips'));
    }

    public function user($user)
    {
        $slips = Slip::with('payment')->whereHas('payment', function ($query) use ($user) {
            return $query->where('user_id', $user);
        })->get();
        $user = User::find($user);
        
        return view('slip::user', compact('slips', 'user'));
    }

    public function create($user)
    {
        return view('slip::create', compact('user'));
    }

    public function check(Request $request, $id)
    {
        $slip = Slip::find($id);
        $slip->status = 'paid';
        $slip->datePaid = $request->get('date');
        $slip->save();

        $s = Slip::with(['user'])->find($id);

        dispatch(new SendMail($s->id, $s->user, 'boletopago'));
        dispatch(new SendSMS($s->id, $s->user, 'boletopago'));

        return redirect()->route('user.panel', $s->user_id);
    }

    public function postback($url, Request $request)
    {
        $input = $request->all();
        $slip = Slip::where('url', $url)->where('transaction', $input['id'])->first();

        if ($slip) {
            if ($input['old_status'] != $input['current_status']) {
                Log::create([
                    'text' => json_encode($input),
                    'type' => $input['current_status'],
                    'slips_id' => $slip->id,
                    'date' => date('Y-m-d H:i:s')
                ]);
            }

            if ($input['current_status'] == 'paid') {
                $slip->status = $input['current_status'];
                $slip->datePaid = date('Y-m-d');
                $slip->save();
                $s = Slip::with(['user'])->find($slip->id);

                dispatch(new SendMail($s->id, $s->user, 'boletopago'));
                dispatch(new SendSMS($s->id, $s->user, 'boletopago'));
            }
        } else {
            Log::create([
                'text' => json_encode($input),
                'type' => $input['current_status'],
                'slips_id' => 0,
                'date' => date('Y-m-d H:i:s')
            ]);
        }
    }

    public function update($id, Request $request)
    {
        try {
            $input = $request->except('_token');
            
            Slip::find($id)->update($input);

            flash('Atualizado com Sucesso', 'success');
        } catch (\Exception $e) {
            flash('Problema ao atualizar Userro '.$e, 'danger');
        }

        return redirect()->route('Slip.index');
    }

    public function destroy($id)
    {
        try {
            $slip = Slip::find($id);
            $slip->delete();

            flash("Apagado!", 'success');
        } catch (\Exception $e) {
            flash("problema ao apagar ".$e, 'danger');
        }

        return redirect()->route('user.panel', $slip->user_id);
    }
}
