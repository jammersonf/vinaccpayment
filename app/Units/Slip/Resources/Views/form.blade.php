<div class="row">
    <div class="col-xs-6 col-md-4">
        {!! Form::label('day', 'Dia pagamento')!!}
        {!! Form::number('day', null, ['class' => 'form-control', 'required']) !!}
    </div>
    <div class="col-xs-6 col-md-4">
        {!! Form::label('value', 'Valor da orferta')!!}
        {!! Form::text('value', null, ['class' => 'form-control', 'required']) !!}
    </div>
</div>