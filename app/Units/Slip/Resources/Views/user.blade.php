@extends('core::layouts.app')

@section('title', 'Mantenedor > Boletos')

@section('page-actions')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Listagem</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Mantenedor</th>
                            <th>Parcela</th>
                            <th>Oferta</th>
                            <th>Vencimento</th>
                            <th>Status</th>
                            <th>Data pagamento</th>
                            <th width="15%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($slips as $s)
                            <tr class="{{ $s->present()->statusClass }}">
                                <td>{{ $s->id }}</td>
                                <td>{{ $s->payment->user->name }}</td>
                                <td>{{ $s->slip }}</td>
                                <td>{{ $s->present()->value }}</td>
                                <td>{{ $s->present()->date }}</td>
                                <td>{{ $s->present()->status }}</td>
                                <td>
                                    @if($s->datePaid)
                                        {{ $s->present()->datePaid }} 
                                    @endif
                                    @if(auth()->user()->isAdmin() && ! $s->datePaid)
                                        {!! Form::open(['method' => 'POST', 'route' => ['slip.check', $s->id]]) !!}
                                        {!! Form::date('date', date('Y-m-d')) !!}
                                        <button type="submit" class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                                        {!! Form::close() !!}
                                    @endif
                                </td>
                                <td class="td-actions">
                                    <a href="{{$s->boleto_url}}" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-print"></i></a>
                                    <a href="{{ route('slip.download', $s->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-download"></i></a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">Vazio</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
@endsection
