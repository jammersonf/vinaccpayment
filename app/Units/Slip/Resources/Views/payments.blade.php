@extends('core::layouts.app')

@section('title', 'Pagamentos')

@section('page-actions')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Filtro</h3>
                </div>
                <div class="panel-body">
                    <div class="col-sm-6 col-md-3">
                        {!! Form::date('date', $date, ['class' => 'form-control date']) !!}
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <button class="btn btn-primary search">Pesquisar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{{count($slips)}} mantenedores</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Mantenedor</th>
                            <th>Parcela</th>
                            <th>Oferta</th>
                            <th>Vencimento</th>
                            <th>Status</th>
                            <th>Data pagamento</th>
                            <th width="15%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($slips as $s)
                            <tr class="{{ $s->present()->statusClass }}">
                                <td>{{ $s->id }}</td>
                                <td>{{ $s->user->name }}</td>
                                <td>{{ $s->slip }}</td>
                                <td>{{ $s->present()->value }}</td>
                                <td>{{ $s->present()->date }}</td>
                                <td>{{ $s->present()->status }}</td>
                                <td>{{ $s->present()->datePaid }}</td>
                                <td class="td-actions">
                                    -
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">Vazio</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
    @include('slip::scripts')
@endsection
