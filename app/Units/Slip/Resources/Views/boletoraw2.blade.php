<html>

<style>
 body
  {
      color:#000000;
      background-color:#ffffff;
      margin-top:0;
      margin-right:0;
  }
  
  table{border:0;border-collapse:collapse;padding:0}
  
  .boleto {
    width: 850px;
    height: 426px;
    border:solid 1px #000;
    margin-top: 3px;
    margin-bottom: 3px;
  }
  .cut{width:850px;height:8px;border-top:dashed 1px #000}
  
  .h1{height:1px}
  .h11{height:11px}
  .h10{height:10px}
  .h11 td{vertical-align:top}
  .h10 td{vertical-align:top}
  
  .w60{width:60px}
  .w160{width:190px}
  .w478{width:478px}
  .w500{width:500px}
  .w638{width:638px}
  .w110{width:110px}
  .w165{width:165px}
  .w65{width:65px}
  .w29{width:29px}
  .w80{width:80px}
  .w108{width:108px}
  .w468{width:468px}
  .w300{width:298px}
  .w178{width:170px}
  
  
  .vias td{ margin-right: 2px;}
  
  .BHead td{border-bottom:solid 2px #000}
  .barra{width:1px;height:10px;;background-color:#000;}
  .Ab{vertical-align:bottom}
  .Ar{text-align:right}
  .Ac{text-align:center}
  .rBb td{border-bottom:solid 1px #000}
  .cpN{font:bold 10px arial;color:black}
  .ctN{font:9px "arial narrow";color:#000033}
  
  .barcode {
    height: 50px;
    margin-top: 10px;
    margin-bottom: 5px;
  }

  .barcode-line {
    height: inherit;
    display: inline-block;
    float: left;
  }
  .ld 
  {
      font: bold 14px arial; 
      color: #000000
  }
  
    .ct 
  {
      font: 9px "arial narrow"; 
      color: #000033
  }
  
  
  .cp 
  {
      font: bold 10px arial; 
      color: black
  }
  
  .ct td, .cp td{padding-left:6px;border-left:solid 1px #000}

  
  .rc6 td{vertical-align:top;border-bottom:solid 1px #000;border-left:solid 1px #000}
  .rc6 div{padding-left:6px}
  .rc6 .t{font:9px "arial narrow";color:#000033;height:13px}
  .rc6 .c{font:bold 10px arial;color:black;height:12px}
   .BB{border-bottom:solid 1px #000}
   
   .EcdBar{height:40px;vertical-align:bottom}
   .Al{text-align:left}

</style>


<body>

    <table class="boleto">
        <tbody>
            
            <tr class="vias">
                <!-- CLIENTE -->
                <td class="w160" style="border-right:solid 1px #000">
                
                
                
                <table class="w160">
                    <tr >
                        <td class="imgLogo Al"><img src="https://assets.pagar.me/boleto/images/bradesco.jpg" /></td>
                    </tr>
                    
                    <tr >
                        <td class="cp">RECIBO SACADOR</td>
                    </tr>
                </table>
                
                    <table class="160">
                        <tr class="ct h11">
                        <td class="w160">N<u>o</u> documento</td>
                        </tr>
                        <tr class="cp h10 rBb">
                            <td class="Ar">{{$data['documento']}}</td>

                        </tr>
                    </table>
                    
                    <table class="160">
                        <tr class="ct h11">
                            <td class="w160">Vencimento</td>
                        </tr>
                        <tr class="cp h10 rBb">
                            <td class="Ar">{{$data['vencimento']}}</td>
                        </tr>
                    </table>
                    
                    <table class="160">
                        <tr class="ct h11">
                            <td class="w160">Agência / Código beneficiário</td>
                        </tr>
                        <tr class="cp h10 rBb">
                            <td class="Ar">{{$data['codigo']}}</td>
                        </tr>
                    </table>
                    
                    <table class="160">
                        <tr class="ct h11">
                            <td class="w160">Carteira / Nosso número</td>
                        </tr>
                        <tr class="cp h10 rBb">
                            <td class="Ar">{{$data['numero']}}</td>
                        </tr>
                    </table>
                    
                    <table class="160">
                        <tr class="ct h11">
                            <td class="w160">(=) Valor documento</td>
                        </tr>
                        <tr class="cp h10 rBb">
                            <td class="Ar">{{$data['valor']}}</td>
                        </tr>
                    </table>
                    
                    <table class="160">
                        <tr class="ct h11">
                            <td class="w160">Vencimento</td>
                        </tr>
                        <tr class="cp h10 rBb">
                            <td class="Ar">{{$data['vencimento']}}</td>
                        </tr>
                    </table>
                    
                    <table>
                    
                    <tr class="rc6">
                    <td class="w160">
                            <div class="t">(-) Desconto / Abatimentos</div>
                            <div class="c BB"></div>
                            <div class="t">(-) Outras deduções</div>
                            <div class="c BB"></div>
                            <div class="t">(+) Mora / Multa</div>
                            <div class="c BB"></div>
                            <div class="t">(+) Outros acréscimos</div>
                            <div class="c BB"></div>
                            <div class="t">(=) Valor cobrado</div>
                            <div class="c"></div>
                        </td>
                    <tr>
                    </table>
                    
                    
                
                
                </td>

             <!-- Loterica -->              
                <td class="w638">
                
                <table class="w638">
                    <tr class="BHead">
                        <td class="imgLogo Al"><img src="https://assets.pagar.me/boleto/images/bradesco.jpg" /></td>
                        <td class="barra"></td>
                        <td class="w60 Ab bc Ac">{{$data['codigoBanco']}}</td>
                        <td class="barra"></td>
                        <td class="w500 Ar Ab ld" id="linha-digitavel-2">{{$data['linhaDigitavel']}}</td>
                    </tr>
                </table>
                
                <table class="w638">
                    <tr class="ct h11">
                        <td class="w478">Local de pagamento</td>
                        <td class="w160">Vencimento</td>
                    </tr>
                    <tr class="cp h10 rBb">
                        <td>PAGÁVEL EM QUALQUER BANCO ATÉ O VENCIMENTO.</td>
                        <td class="Ar">{{$data['vencimento']}}</td>
                    </tr>
                </table>
                
                <table class="w638">
                    <tr class="ct h11">
                        <td class="w478">Beneficiário</td>
                        <td class="w160">Agência / Código beneficiário</td>
                    </tr>
                    <tr class="cp h10 rBb">
                        <td> ENCONTRO PARA A CONSCIENCIA CRISTA | Pagar.me Pagamentos S/A</td>
                        <td class="Ar">{{$data['codigo']}}</td>
                    </tr>
                </table>
                
                <table class="w638">
                    <tr class="ct h11">
                        <td class="w110">Data do documento</td>
                        <td class="w160">N<u>o</u> documento</td>
                        <td class="w65">Espécie doc.</td>
                        <td class="w29">Aceite</td>
                        <td class="w80">Data processamento</td>
                        <td class="w160">Carteira / Nosso número</td>
                    </tr>
                    <tr class="cp h10 rBb">
                        <td>{{$data['data']}}</td>
                        <td>{{$data['documento']}}</td>
                        <td>{{$data['especie']}}</td>
                        <td>{{$data['aceite']}}</td>
                        <td>{{$data['processamento']}}</td>
                        <td class="Ar">{{$data['numero']}}</td>
                    </tr>
                </table>
                
                
                <table class="w638">
                    <tr class="ct h11">
                        <td class="w110">Uso do banco</td>
                        <td class="w80">Carteira</td>
                        <td class="w65">Espécie</td>
                        <td class="w108">Quantidade</td>
                        <td class="w80">(x) Valor</td>
                        <td class="w160">(=) Valor documento</td>
                    </tr>
                    <tr class="cp h12 rBb">
                        <td>&nbsp;</td>
                        <td class="Al">{{$data['carteira']}}</td>
                        <td class="Al">R$</td>
                        <td></td>
                        <td></td>
                        <td class="Ar">{{$data['valor']}}</td>
                    </tr>
                </table>
                
                
                <table class="w638">
                    <tr class="rc6">
                        <td class="w468">
                            <div class="ctN pL10">Instruções (Texto de responsabilidade do beneficiário)</div>
                            <div class="cpN pL10">{{$data['instrucoes']}}</div>
                        </td>
                        <td class="w160">
                            <div class="t">(-) Desconto / Abatimentos</div>
                            <div class="c BB"></div>
                            <div class="t">(-) Outras deduções</div>
                            <div class="c BB"></div>
                            <div class="t">(+) Mora / Multa</div>
                            <div class="c BB"></div>
                            <div class="t">(+) Outros acréscimos</div>
                            <div class="c BB"></div>
                            <div class="t">(=) Valor cobrado</div>
                            <div class="c"></div>
                        </td>
                    </tr>
                </table>
                
                 <table class="w638">
                    <tr class="ct h11">
                            <td class="w300">Pagador</td>
                            <td class="w178">Email</td>
                            <td class="w160">CPF</td>
                         </tr>
                    <tr class="cp h10 rBb">
                        <td>{{ $data['pagador'][0] }}</td>
                        <td>{{ $data['pagador'][1] }}</td>
                        <td>{{ $data['pagador'][2] }}</td>
                    </tr>
                </table>
                
                  <table class="w638 ctN">
                    <tr>
                        <td class="w438">Sacador / Avalista</td>
                        <td class="w160 Ar">Autenticação mecânica - <b class="Ar">Ficha de Compensação</b></td>
                    </tr>
                    
                 </table>
                
                <table class="w666" align="center">
                        <tr>
                          <td class="EcdBar Al pL10">
                            
                              <div class="barcode">
                                {!! $data['codigoBarras'] !!}
                              </div>
                            
                          </td>
                        </tr>
                </table>
                </td>
            </tr>
        </tbody>
    </table>
    
</body>
</html>
