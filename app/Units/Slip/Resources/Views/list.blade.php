@extends('core::layouts.app')

@section('title', 'Ofertas Carnês')

@section('page-actions')
    <a href="{{ route('ticket.create', date('m', strtotime($dt))) }}" class="btn btn-primary">Etiquetas</a>
    <a href="{{ route('slip.payment') }}" class="btn btn-primary">Relatórios</a>
    <a href="{{ route('user.pendencies') }}" class="btn btn-primary">Pendências</a>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Filtros</h3>
            </div>
            <div class="panel-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    {!! Form::open(['route' => 'slip.search']) !!}
                        <div class="input-group">
                            <input type="date" name="date1" class="form-control date" style="width: 170px" value="{{$dt}}" />
                            <input type="date" name="date2" class="form-control date" style="width: 170px" value="{{$dt2 or ''}}" />
                            {!! Form::select('type', ['pagas' => 'Pagas', 'atrasadas' => 'Em atraso'], $type, ['class' => 'form-control', 'style' => 'width: 170px;']) !!}
                            {!! Form::checkbox('vale', $vale, $vale)!!} Vale Oferta
                            <button class="btn btn-primary search" type="submit">Pesquisar</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @if($render)
        <div id="finances-div"></div>
        @combochart('Boletos', 'finances-div')
    @endif

    <div class="row">
    <?php $total = 0; $id = 1;?>
    @foreach($slips as $key => $slip)
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><b>{{date('d/m/Y', strtotime($key))}}</b></h3>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead class="primary">
                    <tr>
                        <th>#</th>
                        <th>Data Pagamento</th>
                        <th>Data Vencimento</th>
                        <th>Mantenedor</th>
                        <th>Parcela</th>
                        <th>Valor</th>
                    </tr>
                </thead>
                <tbody>
                <?php $totalDay = 0; ?>
                @foreach($slip as $s)
                    @php
                        $class = $s->slip == 12 ? 'warning' : '';
                    @endphp
                    <tr>
                        <td>{{$id}}</td>
                        <td>{{ $s->present()->datePaid }}</td>
                        <td>{{ $s->present()->date }}</td>
                        <td>{{ $s->user->name }}</td>
                        <td class="{{$class}}">{{ $s->slip.'/12' }}</td>
                        <td>{{ $s->present()->value}}</td>
                        <?php $totalDay += $s->value; $id++; ?>
                    </tr>
                @endforeach
                <?php $total += $totalDay; ?>
                <tr class="success">
                    <td></td><td></td><td></td>
                    <td align="right">Total</td>
                    <td>{{number_format($totalDay, 2, ',', '.')}}</td>
                </tr>
                </tbody>
            </table>
        </div>
        </div>
        </div>
    @endforeach

        <div class="col-md-6">
            <button class="btn btn-primary btn-block">Total: {{number_format($total, 2, ',', '.')}} | Parcelas: {{$id}}</button>
        </div>
    </div>
</div>
@stop
