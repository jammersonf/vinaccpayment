@section('scripts')
<script type="text/javascript">
;(function($)
{
    'use strict';

    $(document).ready(function() {
    	$(".search").click(function() {
    		window.location.href = 
    		'{{ route('slip.payment') }}/'+$(".date").val();
    	});
    });
})(window.jQuery);
</script>
@endsection