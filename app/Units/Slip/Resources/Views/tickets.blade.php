<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        .test {
            border-style: solid;
            font-size: 9px;
            padding: 1px;
        }
    </style>
</head>
<body>
        @foreach($slips as $s)
            <div class="col-xs-4 test">
                <?php 
                    $user = explode(' - ', $s->user->name); 
                    $user = isset($user[1]) ? $user[1] : $s->user->name;
                ?>
                <div class="col-xs-12" style="padding: 0;"><b>{{ $user }}</b></div>
                <div class="col-xs-12" style="padding: 0;">{{ $s->user->street.', '.$s->user->number}}</div>
                <div class="col-xs-12" style="padding: 0;"><b>{{ $s->user->neighborhood}}</b></div>
                <div class="col-xs-4" style="padding: 0;"><b>{{ $s->user->cep }}</b></div>
                <div class="col-xs-8">{{ $s->user->city }}</div>
            </div>
        @endforeach
</body>
</html>
