<?php

namespace App\Units\Slip\Services;

class SlipService
{
    public function mount($url)
    {
        $dom = new \PHPHtmlParser\Dom();
        $dom->loadFromUrl($url);
        $table = $dom->find('table');
        $arr['header']  = ['html' => $table[2],  'keys' => ['codigoBanco' => 2, 'linhaDigitavel' => 4]];
        $arr['body0']   = ['html' => $table[10], 'keys' => ['local' => 2, 'vencimento' => 3]];
        $arr['body1']   = ['html' => $table[11], 'keys' => ['beneficiario' => 2, 'codigo' => 3]];
        $arr['body2']   = ['html' => $table[12], 'keys' => ['data' => 6, 'documento' => 7, 'especie' => 8, 'aceite' => 9, 'processamento' => 10, 'numero' => 11]];
        $arr['body3']   = ['html' => $table[13], 'keys' => ['carteira' => 7, 'valor' => 11]];
        $arr['body4']   = ['html' => $table[14], 'keys' => ['instrucoes' => 0]];
        $arr['pagador'] = ['html' => $table[6],  'keys' => ['pagador' => 1]];
        $array = [];
        foreach ($arr as $key => $a) {
            $keys = $a['keys'];
            foreach ($keys as $key => $k) {
                $array[$key] = $a['html']->find('td')[$k]->innerHtml;
            }
        }
        $array['instrucoes'] = str_replace(['<div class="ctN pL10">Instruções (Texto de responsabilidade do beneficiário)</div> <div class="cpN pL10">', '</div>'], '', $array['instrucoes']);
        $array['pagador'] = explode('<br />', $array['pagador']);
        $array['codigoBarras'] = $dom->find('.barcode')[0]->outerHtml;
        
        return $array;
    }
}
