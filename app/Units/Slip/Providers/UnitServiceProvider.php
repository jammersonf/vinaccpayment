<?php

namespace App\Units\Slip\Providers;

use Codecasts\Support\Units\ServiceProvider;

class UnitServiceProvider extends ServiceProvider
{
    
    protected $alias = 'slip';

    protected $hasViews = true;

    protected $hasTranslations = true;

    protected $providers = [
        EventServiceProvider::class,
        RouteServiceProvider::class,
    ];
}