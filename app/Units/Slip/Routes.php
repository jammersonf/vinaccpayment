<?php

namespace App\Units\Slip;

use Codecasts\Support\Http\Routing\RouteFile;

class Routes extends RouteFile
{
    /**
     * Declare Web Routes.
     */
    public function routes()
    {
        $this->router->get('slip/{url?}', 'SlipController@postback')->name('slip.postback');
        $this->router->post('slip/{url?}', 'SlipController@postback')->name('slip.postback');
        $this->accessRoutes();
    }

    protected function accessRoutes()
    {
        $this->router->group(['prefix' => 'ofertas'], function () {
            $this->router->get('/', 'SlipController@index')->name('slip.index');
            $this->router->get('{id}/listar', 'SlipController@show')->name('slip.show');
            $this->router->post('{id}/baixa', 'SlipController@check')->name('slip.check');
            $this->router->get('{id}/download', 'SlipController@download')->name('slip.download');
            $this->router->get('{id}/apagar', 'SlipController@destroy')->name('slip.destroy');
            $this->router->get('mantenedor/{user}', 'SlipController@user')->name('slip.user');
            $this->router->get('pagamentos/{date?}', 'SlipController@payments')->name('slip.payment');
            $this->router->post('search', 'SlipController@search')->name('slip.search');
            $this->router->get('{type}', 'SlipController@list')->name('slip.list');

            $this->router->group(['prefix' => 'ticket'], function () {
                $this->router->get('criar/{month?}', 'SlipController@createTicket')->name('ticket.create');
            });
        });
    }
}
