@extends('core::layouts.app')

@section('title', 'Pendência')

@section('page-actions')
    <a href="{{ url()->previous() }}" class="btn btn-fill btn-primary">Voltar</a>
@endsection

@section('content')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lista</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Motoqueiro</th>
                        <th>Mantenedor</th>
                        <th>Solicitação</th>
                        <th>Status</th>
                        <th>Data</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $offero->id }}</td>
                        <td>{{ $offero->rider->name }}</td>
                        <td>{{ $offero->maintainer->name.' - '.$offero->maintainer->phone }}</td>
                        @php
                            $months = $offero->type == 'suspensao' ? $offero->months.' meses' : '';
                        @endphp
                        <td>{{ $offero->type.' '.$months }}</td>
                        <td>{{ $offero->status }}</td>
                        <td>{{ date('d/m/Y', strtotime($offero->date)) }}</td>
                    </tr>
                    <tr>
                        <td></td><td></td>
                        <td align="center"><a href="{{route('offerp.finish', [$offero->id, 'confirmar'])}}" class="btn btn-primary">Confirmar solicitação</a> </td>
                        <td align="center"><a href="{{route('offerp.finish', [$offero->id, 'cancelar'])}}" class="btn btn-warning">Cancelar solicitação</a> </td>
                        <td></td><td></td>
                    </tr>
                </tbody>
            </table>
            </div>
        </div>
    </div>
    <div class="row">
        @include('offerpayment::logs', ['type' => 'status', 'offero' => $offero->id]);
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
;(function($)
{
    'use strict';

    $(document).ready(function() {
        $(".delete").click(function () {
            var r = confirm("Deseja mesmo executar essa ação?");
            if (! r) {
                return false;
            }
        });
    });
})(window.jQuery);
</script>
@endsection