@extends('core::layouts.app')

@section('title', 'Ofertas PAP')

@section('page-actions')
    <a href="{{ route('offer.report') }}" class="btn btn-primary">Relatórios</a>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Filtros</h3>
            </div>
            <div class="panel-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    {!! Form::open(['route' => 'offer.payment.search']) !!}
                        <div class="input-group">
                            <input type="date" name="date1" class="form-control date" style="width: 170px" value="{{$dt}}" />
                            <input type="date" name="date2" class="form-control date" style="width: 170px" value="{{$dt2 or ''}}" />
                            {!! Form::select('type', ['pago' => 'Pagas', 'pendente' => 'Em atraso'], $type, ['class' => 'form-control', 'style' => 'width: 170px;']) !!}
                            {!! Form::select('rider', $riders, $rider, ['class' => 'form-control', 'style' => 'width: 170px;']) !!}
                            {!! Form::select('list', $listArr, $list, ['class' => 'form-control', 'style' => 'width: 100px;']) !!}
                            <button class="btn btn-primary search" type="submit">Pesquisar</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
    <?php $total = 0; $countTotal = 0; ?>
    @foreach($data as $key => $slip)
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><b>{{date('d/m/Y', strtotime($key))}}</b></h3>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead class="primary">
                    <tr>
                        <th>Data Pagamento</th>
                        <th>Data Vencimento</th>
                        <th>Mantenedor</th>
                        <th>Bairro</th>
                        <th>Oferta</th>
                        <th>Valor</th>
                    </tr>
                </thead>
                <tbody>
                @php 
                    $totalDay = 0; 
                    $count = 0;
                @endphp
                @foreach($slip as $s)
                    <tr>
                        @php
                            $date = $s->paid ? date('d/m/Y', strtotime($s->paid)) : '';
                            
                            if (! $s->maintainer) {
                                dd($s);
                            }
                        @endphp

                        <td>{{ $date }}</td>
                        <td>{{ date('d/m/Y', strtotime($s->date)) }}</td>
                        <td>{{ $s->maintainer->name }}</td>
                        <td>{{ $s->maintainer->neighborhood }}</td>
                        <td>{{ $s->number.'/12' }}</td>
                        @php
                            $value = $s->value ? $s->value : $s->maintainer->value;
                            $totalDay += $value;
                            $count++;
                            $countTotal++;
                        @endphp
                        <td>{{ number_format($value, 2, ',', '.') }}</td>
                    </tr>
                @endforeach
                <?php $total += $totalDay; ?>
                <tr class="success">
                    <td></td><td></td>
                    <td>Ofertas: {{$count}}</td>
                    <td align="right">Total</td>
                    <td>{{number_format($totalDay, 2, ',', '.')}}</td>
                </tr>
                </tbody>
            </table>
        </div>
        </div>
        </div>
    @endforeach

        <div class="col-md-6">
            <button class="btn btn-primary btn-block">Total: {{$countTotal}} / {{number_format($total, 2, ',', '.')}}</button>
        </div>
    </div>
</div>
@stop
