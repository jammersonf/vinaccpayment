<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Observações</h3>
        </div>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Data</th>
              <th scope="col">Obs</th>
            </tr>
          </thead>
          <tbody>
            @foreach($logs as $log)
            <tr>
              <th scope="row">{{$log->id}}</th>
              <td>{{date('d/m/Y', strtotime($log->date))}}</td>
              <td>{{ $log->obs }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Observação</h3>
        </div>
        <div class="panel-body">
        {!! Form::open(['route' => ['offerp.obs', $offer->id]]) !!}
          {{Form::hidden('type', $type)}}
          {{Form::hidden('offero', $offero)}}
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="input-group">
                {!! Form::textarea('obs', '', ['class' => 'form-control', 'cols' => 100]) !!}
            </div>
                <button class="btn btn-default btn-sm search" type="submit">Pesquisar</button>
            </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>