@extends('core::layouts.app')

@section('title', 'Pendências')

@section('page-actions')
    <a href="{{ url()->previous() }}" class="btn btn-fill btn-primary">Voltar</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Lista</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Motoqueiro</th>
                            <th>Mantenedor</th>
                            <th>Solicitação</th>
                            <th>Status</th>
                            <th>Data</th>
                            <th>Opções</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($data as $d)
                            <tr>
                                <td>{{ $d->id }}</td>
                                <td>{{ $d->rider->name }}</td>
                                <td>{{ $d->maintainer->name.' - '.$d->maintainer->phone }}</td>
                                @php
                                    $months = $d->type == 'suspensao' ? $d->months.' meses' : '';
                                @endphp
                                <td>{{ $d->type.' '.$months }}</td>
                                <td>{{ $d->status }}</td>
                                <td>{{ date('d/m/Y', strtotime($d->date)) }}</td>
                                <td><a href="{{route('offerp.statusShow', $d->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">Vazio</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
;(function($)
{
    'use strict';

    $(document).ready(function() {
        $(".delete").click(function () {
            var r = confirm("Deseja mesmo executar essa ação?");
            if (! r) {
                return false;
            }
        });
    });
})(window.jQuery);
</script>
@endsection