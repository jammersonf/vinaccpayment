@extends('core::layouts.app')

@section('title', "Oferta")

@section('page-actions')
    <a href="{{url('/motoqueiros/'.$rider.'/payment/'.$list.'#'.$offer->maintainer->neighborhood.$offer->maintainer->id)}}" class="btn btn-fill btn-primary">Voltar</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Oferta {{$offer->number}}/12 | R$ {{number_format($offer->maintainer->value, 2, ',', '.')}}</h3>
                        </div>
                        <div class="panel-body">
                        {!! Form::open(['route' => ['offerp.paid', $offer->id]]) !!}
                            <div class="col-xs-8 col-sm-8 col-md-6 col-lg-6">
                                <div class="input-group">
                                    {!! Form::label('value', 'Pago')!!}    
                                    {!! Form::text('value', str_replace('.00', '', $offer->maintainer->value), ['class' => 'form-control'])!!}  
                                </div>
                            </div>
                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                <div class="input-group">
                                {!! Form::label('submit', '-')!!} <br>   
                                <button class="btn btn-default btn-sm search" type="submit">Enviar</button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Reagendar</h3>
                        </div>
                        <div class="panel-body">
                        {!! Form::open(['route' => ['offerp.reschedule', $offer->id]]) !!}
                            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                    {!! Form::label('date', 'Data')!!}    
                                    {!! Form::date('date', date('Y-m-d'), ['class' => 'form-control'])!!}  
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                {!! Form::label('submit', '-')!!} <br>   
                                <button class="btn btn-default btn-sm search" type="submit">Enviar</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Opções</h3>
                        </div>
                        <div class="panel-body">
                        {!! Form::open(['route' => ['offerp.option', $offer->id]]) !!}
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                {!! Form::label('months', 'Meses')!!}    
                                {!! Form::number('months', 1, ['class' => 'form-control month'])!!}  
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                {!! Form::label('status', 'Status')!!}    
                                {!! Form::select('status', ['suspensao' => 'Suspensão', 'proxmes' => 'Prox mês', 'cancelamento' => 'Cancelamento'], 'suspensao', ['class' => 'form-control status'])!!}  
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                {!! Form::label('submit', '-')!!} <br>   
                                <button class="btn btn-default btn-sm search" type="submit">Enviar</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            @include('offerpayment::logs', ['type' => 'offer', 'offero' => 0])
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
;(function($)
{
    'use strict';

    $(document).ready(function() {
        $(".delete").click(function () {
            var r = confirm("Deseja mesmo executar essa ação?");
            if (! r) {
                return false;
            }
        });
        $('.status').change(function() {
            if ($('.status').val() === 'Suspensao') {
                $('.month').show();
            } else {
                $('.month').hide();
            }
        })
    });
})(window.jQuery);
</script>
@endsection
