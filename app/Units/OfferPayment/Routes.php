<?php

namespace App\Units\OfferPayment;

use Codecasts\Support\Http\Routing\RouteFile;

class Routes extends RouteFile
{
    /**
     * Declare Web Routes.
     */
    public function routes()
    {
        $this->accessRoutes();
    }

    protected function accessRoutes()
    {
        $this->router->get('ofertas/detalhes', 'OfferPaymentController@index')->name('offer.payment.index');
        $this->router->get('ofertaspap', 'OfferPaymentController@payments')->name('offer.payment.payments');
        $this->router->post('ofertaspap', 'OfferPaymentController@search')->name('offer.payment.search');

        $this->router->group(['prefix' => 'oferta'], function () {
            $this->router->get('status', 'OfferOptionController@status')->name('offerp.status');
            $this->router->get('status/{id}', 'OfferOptionController@statusShow')->name('offerp.statusShow');
            $this->router->get('status/{id}/{status}', 'OfferOptionController@finish')->name('offerp.finish');
            $this->router->get('{id}/show', 'OfferPaymentController@show')->name('offerp.show');
            $this->router->post('{id}/update', 'OfferPaymentController@update')->name('offerp.update');
            $this->router->post('{id}/pagar', 'OfferOptionController@paid')->name('offerp.paid');
            $this->router->post('{id}/reagendar', 'OfferOptionController@reschedule')->name('offerp.reschedule');
            $this->router->post('{id}/option', 'OfferOptionController@option')->name('offerp.option');
            $this->router->post('{id}/obs', 'OfferPaymentController@obs')->name('offerp.obs');
            $this->router->get('{id}/reset', 'OfferPaymentController@reset')->name('offerp.reset');
        });
    }
}
