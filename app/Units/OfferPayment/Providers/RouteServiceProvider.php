<?php

namespace App\Units\OfferPayment\Providers;

use App\Units\OfferPayment\Routes;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Units\OfferPayment\Http\Controllers';

    public function boot()
    {
        parent::boot();
    }

    public function map()
    {
        (new Routes([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ]))->register();
    }
}
