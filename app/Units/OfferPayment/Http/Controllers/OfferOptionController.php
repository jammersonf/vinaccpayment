<?php

namespace App\Units\OfferPayment\Http\Controllers;

use App\Domains\OfferPaymentLogs\OfferPaymentLog;
use App\Domains\OfferPaymentOption\OfferPaymentOption;
use App\Domains\OfferPaymentReschedule\OfferPaymentReschedule;
use App\Domains\OfferPayments\OfferPayment;
use App\Domains\Offers\Offer;
use App\Domains\Users\User;
use App\Units\Core\Jobs\SendMailPAP;
use App\Units\Core\Jobs\SendSMSPAP;
use Carbon\Carbon;
use Codecasts\Support\Http\Controller;
use Illuminate\Http\Request;

class OfferOptionController extends Controller
{
    protected $offerp;
    protected $offerl;
    protected $offerre;
    protected $offero;
    protected $user;

    public function __construct(OfferPayment $offerp, OfferPaymentLog $opl, OfferPaymentReschedule $ofp, User $u, OfferPaymentOption $opo)
    {
        $this->middleware('auth');
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        }
        $this->offerp = $offerp;
        $this->offero = $opo;
        $this->offerl = $opl;
        $this->offerre = $ofp;
        $this->user = $u;
    }

    public function status()
    {
        $data = $this->offero
                     ->with(['maintainer', 'rider'])
                     ->unresolved()
                     ->get();

        return view('offerpayment::status', compact('data'));
    }

    public function paid($id, Request $request)
    {
        $input = $request->all();
        $offerp = $this->offerp->find($id);
        $offer = Offer::find($offerp->offer_id);
        $offerp->value = $request->get('value');
        $offerp->paid = isset($input['date']) ? $input['date'] : date('Y-m-d H:i:s');
        $offerp->status = 'pago';
        $offerp->save();

        if (config('app.send')) {
            dispatch(new SendMailPAP($offer->maintainer, 'recibopap', $offerp));
            dispatch(new SendSMSPAP($offer->maintainer, 'recibopap', $offerp));
        }

        return redirect()->route('offer.print', $offerp->id);
    }

    public function statusShow($id)
    {
        $offero = $this->offero->find($id);
        $offer = $offero->offerp;
        
        $logs = $this->offerl->byPayment($offero->offerp->id)->get();

        return view('offerpayment::statusShow', compact('offero', 'offer', 'logs'));
    }

    public function reschedule($id, Request $request)
    {
        $date = $request->get('date');
        $offerp = $this->offerp->find($id);
        $offer = $offerp->offer;
        $dt1 = Carbon::createFromFormat('Y-m-d', $date);
        $dt2 = Carbon::createFromFormat('Y-m-d', $offerp->date)->addDays(26);
        if ($dt1->greaterThan($dt2)) {
            flash('Não pode ser reagendado nessa data. O máximo é 26 dias após a data da lista!', 'danger');
            return redirect()->route('offerp.show', $id);
        }

        $this->offerre->create([
            'offer_payment_id' => $id,
            'rider_id' => $offer->rider_id,
            'list' => $offer->list,
            'date' => $date
        ]);

        $log = "Oferta adiada para o dia ".date('d/m/Y', strtotime($date));
        $this->offerl->newLog($id, $log);

        if (config('app.send')) {
            dispatch(new SendMailPAP($offer->maintainer, 'reagendamento', $offerp, $date));
            dispatch(new SendSMSPAP($offer->maintainer, 'reagendamento', $offerp, $date));
        }

        flash('Oferta Reagendada!', 'success');

        return redirect(url('/motoqueiros/'.$offer->rider_id.'/payment/'.$offer->list.'#'.$offer->maintainer->neighborhood.$offer->maintainer->id));
    }

    public function option($id, Request $request)
    {
        $input = $request->all();
        $offerp = $this->offerp->find($id);
        $offer = $offerp->offer;

        if ($input['status'] == 'proxmes') {
            $this->proxmes($offerp, $input);
        } else {
            $this->createOption($offerp, $input);
        }

        $log = "motoqueiro muda status para: ".$input['status'];
        $this->offerl->newLog($offerp->id, $log);

        flash('Feito!', 'success');
        
        return redirect(url('/motoqueiros/'.$offer->rider_id.'/payment/'.$offer->list.'#'.$offer->maintainer->neighborhood.$offer->maintainer->id));
    }

    public function proxmes($offerp, $input)
    {
        $this->offerp->suspend($offerp, $input['months']);
        
        if (config('app.send')) {
            dispatch(new SendMailPAP($offerp->maintainer, 'proximomes'));
            dispatch(new SendSMSPAP($offerp->maintainer, 'proximomes'));
        }
    }

    public function createOption($offerp, $input)
    {
        $offer = $offerp->offer;

        $this->offero->create([
            'offer_payment_id' => $offerp->id,
            'maintainer_id' => $offer->maintainer_id,
            'rider_id' => $offer->rider_id,
            'type' => $input['status'],
            'months' => $input['status'] == 'suspensao' ? $input['months'] : 0 ,
            'status' => 'pendente'
        ]);
    }

    public function finish($id, $status)
    {
        $offero = $this->offero->find($id);
        if ($status == 'confirmar') {
            if ($offero->type == 'suspensao') {
                $this->offerp->suspend($offero->offerp, $offero->months);
            } else {
                $this->offerp->cancel($offero->offerp);
            }
            if (config('app.send')) {
                dispatch(new SendMailPAP($offero->maintainer, $offero->type));
                dispatch(new SendSMSPAP($offero->maintainer, $offero->type));
            }
        }
        $offero->retorno = $status;
        $offero->status = 'resolvido';
        $offero->save();

        $log = "admin muda status para: resolvido";
        $this->offerl->newLog($offero->offerp->id, $log);
        
        flash('Feito!', 'success');
        return redirect()->route('offerp.status');
    }
}
