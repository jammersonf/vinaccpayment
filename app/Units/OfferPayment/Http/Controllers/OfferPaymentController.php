<?php

namespace App\Units\OfferPayment\Http\Controllers;

use App\Domains\OfferPaymentLogs\OfferPaymentLog;
use App\Domains\OfferPaymentReschedule\OfferPaymentReschedule;
use App\Domains\OfferPayments\OfferPayment;
use App\Domains\Users\User;
use Codecasts\Support\Http\Controller;
use Illuminate\Http\Request;

class OfferPaymentController extends Controller
{
    protected $offerp;
    protected $offerl;
    protected $offerre;
    protected $user;

    public function __construct(OfferPayment $offerp, OfferPaymentLog $opl, OfferPaymentReschedule $ofp, User $u)
    {
        $this->middleware('auth');
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        }
        $this->offerp = $offerp;
        $this->offerl = $opl;
        $this->offerre = $ofp;
        $this->user = $u;
    }
    
    public function index()
    {
        return view('offerpayment::index');
    }

    public function show($id)
    {
        $offer = $this->offerp->find($id);
        $logs = $this->offerl->byPayment($id)->get();
        $rider = $offer->offer->rider_id;
        $list = $offer->offer->list;

        return view('offerpayment::show', compact('offer', 'logs', 'rider', 'list'));
    }

    public function update($id, Request $request)
    {
        $input = $request->except(['_token']);
        $offerp = $this->offerp->find($id);
        $offerp->date = $input['date'];
        $offerp->date_old = $input['date'];
        $offerp->save();

        flash('Data Alterada!');
        return redirect()->route('offer.show', $offerp->offer_id);
    }

    public function reset($id)
    {
        $offerp = $this->offerp->find($id);
        $this->offerp->where('id', $id)->update(['status' => 'pendente', 'paid' => NULL]);

        flash('Oferta resetada!');
        return redirect()->route('offer.show', $offerp->offer_id);
    }

    public function payments()
    {
        $dt = date('Y-m-01');
        $dt2 = date('Y-m-d');
        $type = 'pago';
        $rider = '';
        $list = '';
        $listArr = ['' => 'Todos', 5 => 5, 10 => 10, 15 => 15, 20 => 20, 25 => 25, 30 => 30];
        $riders = ['' => 'Motoqueiro'] + $this->user->rider()->get()->pluck('name', 'id')->toArray();
        
        $data = $this->offerp->getPayments($dt, $dt2, $type, $list, $rider);
        
        return view('offerpayment::payments', compact('data', 'dt', 'dt2', 'type', 'riders', 'rider', 'list', 'listArr'));
    }

    public function search(Request $request)
    {
        $input = $request->all();

        $dt = $input['date1'];
        $dt2 = $input['date2'];
        $type = $input['type'];
        $rider = $input['rider'];
        $list = $input['list'];
        $listArr = ['' => 'Lista', 5 => 5, 10 => 10, 15 => 15, 20 => 20, 25 => 25, 30 => 30];
        $riders = ['' => 'Motoqueiro'] + $this->user->rider()->get()->pluck('name', 'id')->toArray();
        $data = $this->offerp->getPayments($dt, $dt2, $type, $list, $rider);

        return view('offerpayment::payments', compact('data', 'dt', 'dt2', 'type', 'rider', 'list', 'listArr', 'riders'));
    }

    public function obs(Request $request, $payment)
    {
        $log = new OfferPaymentLog();
        $log->offer_payment_id = $payment;
        $log->obs = $request->get('obs');
        $log->save();

        if ($request->get('type') == 'offer') {
            return redirect()->route('offerp.show', $payment);
        } else {
            return redirect()->route('offerp.statusShow', $request->get('offero'));
        }
    }
}
