<?php

namespace App\Units\Auth\Http\Controllers;

use Codecasts\Support\Http\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/';

    public function home()
    {
        if (auth()->check()) {
            if (auth()->user()->isAdmin()) {
                return redirect()->route('user.index');
            } elseif (auth()->user()->profile == 'motoqueiro') {
                return redirect()->route('user.rider.panel', auth()->user()->id);
            }
            return redirect()->route('user.panel', auth()->user()->id);
        }
        return view('auth::login');
    }

    public function login(Request $request)
    {
        $input = $request->all();
        $login = $input['email'];
        if (auth()->attempt(['email' => $login, 'password' => $input['password']])) {
            if (auth()->user()->status == 0) {
                auth()->logout();
                session()->flash('message-class', 'warning');
                return redirect()->route('index')->with('message', 'Seu perfil ainda está em análise. Volte em instantes');
            }

            if (auth()->user()->isAdmin()) {
                return redirect()->route('user.index');
            } elseif (auth()->user()->profile == 'motoqueiro') {
                return redirect()->route('user.rider.panel', auth()->user()->id);
            }
            return redirect()->route('user.panel', auth()->user()->id);
        }

        session()->flash('message-class', 'danger');
        return redirect()->route('index')->with('message', 'Os dados não conferem, tente novamente');
    }

    public function logout()
    {
        auth()->logout();
        session()->flush();
        return redirect('/');
    }
}
