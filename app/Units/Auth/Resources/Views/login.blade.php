@extends('auth::app')

@section('title', 'Login')

@section('content')
    <h3 class="text-center">{{ config('app.name') }}</h3>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif
    <br>
    {!! Form::open(['route' => 'login']) !!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="no-label">
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required']) !!}
    </div>
    <p>
    <div class="no-label">
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Senha', 'required']) !!}
        <h6>A senha é os digítos do seu CPF</h6>
    </div>
    <br>
    {!! Form::submit('Login', ['class' => 'btn btn-primary btn-lg btn-block form-action']) !!}
    <a href="{{route('user.create')}}" class="btn btn-primary btn-lg btn-block form-action">Não tem login? Cadastre-se</a>
    {!! Form::close() !!}
    <br>
    <br>
@stop
