@extends('auth.app')

@section('content')
    <img src="{!! Auth::user()->avatar !!}" alt="avatar" class="img-circle profile-image center-block">
    <h3 class="text-center">Olá, {{ Auth::user()->name}}.</h3>
    <br>
    <h4 class="text-center">Preciso de algumas informações: </h4>

    {!! Form::open(['to' => route('store.password')]) !!}
    <div class="no-label">
        {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Telefone', 'data-mask' => '(99) 99999-9999', 'required']) !!}
    </div>
    <p>
    <div class="no-label">
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Senha', 'required']) !!}
    </div>
    <p>
    <div class="no-label">
        {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirmar senha', 'required']) !!}
    </div>
    <br>
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary btn-lg btn-block form-action']) !!}
    {!! Form::close() !!}
@stop
